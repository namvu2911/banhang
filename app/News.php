<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model {
	protected $table = "news";
	public function categoryNews() {
		return $this->belongsTo('App\News', 'cat_news_id', 'id');
	}
}
