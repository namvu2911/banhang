<?php

namespace App\Http\Controllers;

use App\CategoryProduct;
use Illuminate\Http\Request;

class CategoryProductController extends Controller {
	public function getList() {
		$cat_pro = CategoryProduct::all();
		return view('backend.categoryproduct.list', compact('cat_pro'));
	}
	public function getAdd() {
		$cat_pro = CategoryProduct::all();
		return view('backend.categoryproduct.add',compact('cat_pro'));
	}
	public function postAdd(Request $request) {
		$this->validate($request,[
                'name'=>'required|min:2|max:100|',
            ],[
                'name.required'=>'Bạn chưa nhập tiêu đề',
                'name.min'=>'Tiêu đề phải từ 2 ký tự trở lên',
                'name.max'=>'Tiêu đề tối đa 100 ký tự',
                'name.unique'=>'Tiêu đề đã tồn tại',
            ]);
		$cate_product = new CategoryProduct;
		$cate_product->name = $request->name;
		$cate_product->slug = str_slug($request->name);
		if($request->hasFile('image')){
			$file = $request->file('image');
            $duoi = $file->getClientOriginalExtension();
            if($duoi != 'jpg' && $duoi != 'png' && $duoi != 'jpeg'){
            	return redirect('admin/categoryproduct/add')->with('error_img','Bạn chọn hình không đúng định dạng');
            }
            $name = $file->getClientOriginalName();
            $hinh = str_random(5)."-".$name;
            while(file_exists("uploads/2/banner-group-product/".$hinh))
            {
                $hinh = str_random(5)."-".$name;
            }
            $file->move("uploads/2/banner-group-product/",$hinh);
            $cate_product->image = $hinh;
		}
		 else
        {
            $cate_product->image = "";
        }
        $cate_product->save();
        return redirect("admin/categoryproduct/list")->with('thongbao','Thêm mục sản phẩm thành công');
	}
	public function getEdit($id) {
		$cate_pro = CategoryProduct::find($id);
		return view("backend/categoryproduct/edit",compact('cate_pro'));
	}
	public function getDel($id) {
		$categoryproduct = CategoryProduct::find($id);
        $categoryproduct->delete();
        return redirect('admin/product/list')->with('thongbao','Bạn đã xóa tài khoản thành công');
	}
	public function postEdit(Request $request,$id) {
		$cate_product = CategoryProduct::find($id);
		$this->validate($request,[
                'name'=>'required|min:2|max:100|',
            ],[
                'name.required'=>'Bạn chưa nhập tiêu đề',
                'name.min'=>'Tiêu đề phải từ 2 ký tự trở lên',
                'name.max'=>'Tiêu đề tối đa 100 ký tự',
                'name.unique'=>'Tiêu đề đã tồn tại',
            ]);
		$cate_product = CategoryProduct::find($id);
		$cate_product->name = $request->name;
		$cate_product->slug = str_slug($request->name);
		if($request->hasFile('image')){
			$file = $request->file('image');
            $duoi = $file->getClientOriginalExtension();
            if($duoi != 'jpg' && $duoi != 'png' && $duoi != 'jpeg'){
            	return redirect('admin/categoryproduct/add')->with('error_img','Bạn chọn hình không đúng định dạng');
            }
            $name = $file->getClientOriginalName();
            $hinh = str_random(5)."-".$name;
            while(file_exists("uploads/2/banner-group-product/".$hinh))
            {
                $hinh = str_random(5)."-".$name;
            }
            $file->move("uploads/2/banner-group-product/",$hinh);
            $cate_product->image = $hinh;
		}
		 else
        {
            $cate_product->image = "";
        }
        $cate_product->save();
        return redirect("admin/categoryproduct/list")->with('thongbao','Thêm mục sản phẩm thành công');
	}
}
