<?php

namespace App\Http\Controllers;
use App\CategoryNews;
use Illuminate\Http\Request;

class CategoryNewsController extends Controller {
	public function getList() {
		$cat_news = CategoryNews::all();
		// echo "<pre>";
		// print_r($cat_news);die;
		return view('backend.categorynews.list',compact('cat_news'));
	}
	public function getAdd() {
		$cat_news = new CategoryNews;
		return view('backend.categorynews.add',compact('cat_news'));
	}
	public function postAdd(Request $request) {
		$cat_news = new CategoryNews;
		$this->validate($request,[
			 'title'=>'required|min:2|max:100|',
            ],[
                'title.required'=>'Bạn chưa nhập tiêu đề',
                'title.min'=>'Tiêu đề phải từ 2 ký tự trở lên',
                'title.max'=>'Tiêu đề tối đa 100 ký tự',
                'title.unique'=>'Tiêu đề đã tồn tại',
           
            ]);
		$cat_news->title = $request->title;
		$cat_news->slug = str_slug($request->title);
		$cat_news->save();
        return redirect("admin/category_news/add")->with('thongbao','Bạn đã thêm mục tin mới thành công');

	}
	public function getEdit($id) {
		$cat_news = CategoryNews::find($id);
		return view("backend.categorynews.edit",compact('cat_news'));		
	}
	public function postEdit(Request $request,$id) {
		$cat_news = CategoryNews::find($id);

		$this->validate($request,[
			 'title'=>'required|min:2|max:100|',
            ],[
                'title.required'=>'Bạn chưa nhập tiêu đề',
                'title.min'=>'Tiêu đề phải từ 2 ký tự trở lên',
                'title.max'=>'Tiêu đề tối đa 100 ký tự',
                'title.unique'=>'Tiêu đề đã tồn tại',
           
            ]);
		$cat_news->title = $request->title;
		$cat_news->slug = str_slug($request->title);
		$cat_news->save();
        return redirect("admin/category_news/add")->with('thongbao','Bạn đã thêm mục tin mới thành công');
	}
	public function getDel($id) {
		$cat_news = CategoryNews::find($id);
		$cat_news->delete();
		return redirect("admin/category_news/add")->with('thongbao','Bạn đã xóa mục tin thành công');

	}
	
}
