<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Banner;

class BannerController extends Controller {
	public function getList() {
		$banner = Banner::all();
		return view('backend.banner.list', compact('banner'));
	}
	public function getAdd() {
		$banner = Banner::all();
		return view('backend.banner.add');
	}
	public function postAdd(Request $request) {
		$banner = new Banner;
		if($request->hasFile('image')){
			$file = $request->file('image');
            $duoi = $file->getClientOriginalExtension();
            if($duoi != 'jpg' && $duoi != 'png' && $duoi != 'jpeg'){
            	return redirect('admin/banner/add')->with('error_img','Bạn chọn hình không đúng định dạng');
            }
            $name = $file->getClientOriginalName();
            $hinh = str_random(5)."-".$name;
            while(file_exists("uploads/2/page-templates/home-1/".$hinh))
            {
                $hinh = str_random(5)."-".$name;
            }
            $file->move("uploads/2/page-templates/home-1/",$hinh);
            $banner->image = $hinh;
		}
		 else
        {
            $banner->image = "";
        }
        $banner->save();
		 return redirect("admin/banner/list")->with('thongbao','Bạn đã thêm giao diện thành công');
	}
	public function getEdit($id) {
		$banner = Banner::find($id);
		return view('backend/banner/edit',compact('banner'));
	}
	public function postEdit(Request $request,$id)
	{
		$banner =  Banner::find($id);
		if($request->hasFile('image')){
			$file = $request->file('image');
            $duoi = $file->getClientOriginalExtension();
            if($duoi != 'jpg' && $duoi != 'png' && $duoi != 'jpeg'){
            	return redirect('admin/banner/add')->with('error_img','Bạn chọn hình không đúng định dạng');
            }
            $name = $file->getClientOriginalName();
            $hinh = str_random(5)."-".$name;
            while(file_exists("uploads/2/page-templates/home-1/".$hinh))
            {
                $hinh = str_random(5)."-".$name;
            }
            $file->move("uploads/2/page-templates/home-1/",$hinh);
            $banner->image = $hinh;
		}
		 else
        {
            $banner->image = "";
        }
        $banner->save();
		 return redirect("admin/banner/list")->with('thongbao','Bạn đã Sửa giao diện thành công');
	}
	public function getDel($id) {
		$banner = Banner::find($id);
		$banner->delete();
		 return redirect("admin/banner/list")->with('thongbao','Bạn đã xóa giao diện thành công');

	}
	
}
