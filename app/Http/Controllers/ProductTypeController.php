<?php

namespace App\Http\Controllers;
use App\ProductType;
use App\CategoryProduct;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ProductTypeController extends Controller {
	public function getList() {
		$type = DB::table('product_type')
	            ->join('category_products', 'product_type.cate_id', '=', 'category_products.id')
	           
	            ->select('product_type.*', 'category_products.name','category_products.parent')
	            ->get();
	    
		return view('backend.producttype.list', compact('type'));
	}
	public function getAdd() {
	$cate_pro = CategoryProduct::all();
		return view('backend.producttype.add',compact('cate_pro'));
	}
	public function postAdd(Request $request) {
		$this->validate($request,[
			'type_name'=>'required|min:3|max:100',
		],[
			'type_name.required'=>'Bạn chưa nhập tên loại sản phẩm',
			'type_name.min'=>'Tiêu đề phải từ 2 ký tự trở lên',
                'type_name.max'=>'Tiêu đề tối đa 100 ký tự',
                'type_name.unique'=>'Tiêu đề đã tồn tại',

		]

	);
		$protype = new ProductType;
		$protype->cate_id = $request->category_product;
		$protype->type_name = $request->type_name;
		$protype->slug = str_slug($request->type_name);
		if($request->hasFile('image')){
			$file = $request->file('image');
            $duoi = $file->getClientOriginalExtension();
            if($duoi != 'jpg' && $duoi != 'png' && $duoi != 'jpeg'){
            	return redirect('admin/product_type/add')->with('error_img','Bạn chọn hình không đúng định dạng');
            }
            $name = $file->getClientOriginalName();
            $hinh = str_random(5)."-".$name;
            while(file_exists("uploads/2/banner-group-product/".$hinh))
            {
                $hinh = str_random(5)."-".$name;
            }
            $file->move("uploads/2/banner-group-product/",$hinh);
            $protype->image = $hinh;
		}
		 else
        {
            $protype->image = "";
        }
        $protype->save();
        return redirect("admin/product_type/list")->with('thongbao','Thêm loại sản phẩm thành công');
	
	}
	public function getEdit($id) {
		$cate_pro = CategoryProduct::all();
		$pro_type = CategoryProduct::find($id);

		return view('backend.producttype.edit',compact('cate_pro','pro_type'));
	}
	public function postEdit(Request $request,$id) {
		$this->validate($request,[
			'type_name'=>'required|min:3|max:100',
		],[
			'type_name.required'=>'Bạn chưa nhập tên loại sản phẩm',
			'type_name.min'=>'Tên loại sản phẩm phải từ 2 ký tự trở lên',
                'type_name.max'=>'Tên loại sản phẩm tối đa 100 ký tự',
                'type_name.unique'=>'Tên loại sản phẩm đã tồn tại',

		]

			);
		$pro_type = ProductType::find($id);
		$pro_type->cate_id = $request->category_product;
		$pro_type->type_name = $request->type_name;
		$pro_type->slug = str_slug($request->type_name);
		if($request->hasFile('image')){
			$file = $request->file('image');
            $duoi = $file->getClientOriginalExtension();
            if($duoi != 'jpg' && $duoi != 'png' && $duoi != 'jpeg'){
            	return redirect('admin/product_type/edit')->with('error_img','Bạn chọn hình không đúng định dạng');
            }
            $name = $file->getClientOriginalName();
            $hinh = str_random(5)."-".$name;
            while(file_exists("uploads/2/banner-group-product/".$hinh))
            {
                $hinh = str_random(5)."-".$name;
            }
            $file->move("uploads/2/banner-group-product/",$hinh);
            $pro_type->image = $hinh;
		}
		 else
        {
            $pro_type->image = "";
        }
        $pro_type->save();
        return redirect("admin/product_type/list")->with('thongbao','Sửa loại sản phẩm thành công');
	}
	public function getDel($id) {
		$pro_type = ProductType::find($id);
		$pro_type->delete();
		return redirect("admin/product_type/list")->with('thongbao','Sửa loại sản phẩm thành công');

	}
}
