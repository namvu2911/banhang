<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\ProductType;
use App\CategoryProduct;
use App\Product;

class ProductController extends Controller {
	public function getList() {
		$product = DB::table('product')
				->join('category_products', 'product.cate_id', '=', 'category_products.id')
	           	->join('product_type','product.type_id','=','product_type.id')
	            ->select('product.*', 'category_products.name','product_type.type_name')
	            ->get();
	   

	    return view('backend.product.list',compact('product'));
	}
	public function getAdd() {
		
		$cate_product =  CategoryProduct::all();
	
		foreach($cate_product as $item){
			$product_type = DB::table('product_type')
							->join('category_products','product_type.cate_id','=','category_products.id')
							->where('product_type.cate_id','=',$item->id)->get();
		}
		
		
	
		return view('backend.product.add',compact('product_type','cate_product'));
	}
	public function postAdd(Request $request) {
		$product = new Product;
		$this->validate($request,[ 
			'product_name'=>'required|min:2|max:100|',

		],[
			'product_name.required'=>'Bạn chưa nhập tiêu đề',
                'product_name.min'=>'Tiêu đề phải từ 2 ký tự trở lên',
                'product_name.max'=>'Tiêu đề tối đa 100 ký tự',
                'product_name.unique'=>'Tiêu đề đã tồn tại',
		]);

	}
	public function getEdit($id) {


		return view('backend.product.edit');
	}
	public function postEdit($id) {

	}
	public function getDel($id) {
		$product = Product::find($id);
		$product->delete();
		return redirect('admin/product/list')->withSuccess('Xóa sản phẩm thành công');
	}
}
