<?php

namespace App\Http\Controllers;
use App\TypeNews;
use App\CategoryNews;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TypeNewsController extends Controller
{
     public function getList() {
     	$type = DB::table('type_news')
     				->join('category_news','type_news.cate_news_id','=','category_news.id')
     				->select('type_news.*','category_news.title')
     				->get();
     	// echo "<pre>";
     	// print_r($type);die;
     	return view('backend.type_news.list',compact('type'));
	}
	public function getAdd() {
		$type = DB::table('type_news')
     				->join('category_news','type_news.cate_news_id','=','category_news.id')
     				->select('type_news.*','category_news.title')
     				->get();
     	$cate_news = CategoryNews::all();
     	return view('backend.type_news.add',compact('type','cate_news'));
	}
	public function postAdd(Request $request) {
		$cate_type = CategoryNews::all();
		$type = new TypeNews;
		$this->validate($request,[
			 'title_type'=>'required|min:2|max:100|',
		],[
			'title_type.required'=>'Bạn chưa nhập tên loại sản phẩm',
			'title_type.min'=>'Tiêu đề phải từ 2 ký tự trở lên',
            'title_type.max'=>'Tiêu đề tối đa 100 ký tự',
            'title_type.unique'=>'Tiêu đề đã tồn tại',
		]);
		$type->cate_news_id = $request->category_news;
		$type->title_type = $request->title_type;
		$type->description = $request->description;
		$type->slug = str_slug($request->title_type);
		if($request->hasFile('image')){
			$file = $request->file('image');
            $duoi = $file->getClientOriginalExtension();
            if($duoi != 'jpg' && $duoi != 'png' && $duoi != 'jpeg'){
            	return redirect('admin/type_news/add')->with('error_img','Bạn chọn hình không đúng định dạng');
            }
            $name = $file->getClientOriginalName();
            $hinh = str_random(5)."-".$name;
            while(file_exists("uploads/5/tin-cong-ty/".$hinh))
            {
                $hinh = str_random(5)."-".$name;
            }
            $file->move("uploads/5/tin-cong-ty/",$hinh);
            $type->image = $hinh;
		}
		 else
        {
            $type->image = "";
        }
        $type->save();
        return redirect("admin/type_news/add")->with('thongbao','Bạn đã thêm tin thành công');
       
	}
	public function getEdit($id) {
		
		$type = TypeNews::find($id);
		$cate_news = CategoryNews::all();

		return view('backend.type_news.edit',compact('cate_news','type'));
	}
	public function postEdit(Request $request,$id){
		$type = TypeNews::find($id);
		$this->validate($request,[
			 'title_type'=>'required|min:2|max:100|',
		],[
			'title_type.required'=>'Bạn chưa nhập tên loại sản phẩm',
			'title_type.min'=>'Tiêu đề phải từ 2 ký tự trở lên',
            'title_type.max'=>'Tiêu đề tối đa 100 ký tự',
            'title_type.unique'=>'Tiêu đề đã tồn tại',
		]);
		$type->cate_news_id = $request->category_news;
		$type->title_type = $request->title_type;
		$type->description = $request->description;
		$type->slug = str_slug($request->title_type);
		if($request->hasFile('image')){
			$file = $request->file('image');
            $duoi = $file->getClientOriginalExtension();
            if($duoi != 'jpg' && $duoi != 'png' && $duoi != 'jpeg'){
            	return redirect('admin/type_news/edit')->with('error_img','Bạn chọn hình không đúng định dạng');
            }
            $name = $file->getClientOriginalName();
            $hinh = str_random(5)."-".$name;
            while(file_exists("uploads/5/tin-cong-ty/".$hinh))
            {
                $hinh = str_random(5)."-".$name;
            }
            $file->move("uploads/5/tin-cong-ty/",$hinh);
            $type->image = $hinh;
		}
		 else
        {
            $type->image = "";
        }
        $type->save();
        return redirect("admin/type_news/list")->with('thongbao','Bạn đã sửa tin thành công');
	}	
	public function getDel($id) {
		$type = TypeNews::find($id);
		$type->delete();
        return redirect("admin/type_news/list")->with('thongbao','Bạn đã xóa tin thành công');

	}
}
