<?php

namespace App\Http\Controllers;
use App\Banner;
use App\CategoryProduct;
use App\ProductType;
use App\CategoryNews;
use App\TypeNews;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class PageController extends Controller {
	public function getIndex() {
		$cat_pro = CategoryProduct::all();
		$banner = Banner::all();

		return view('frontend.page.trangchu', compact('cat_pro', 'banner'));
	}
	public function getLoaiSanPham() {

		$cat_pro = CategoryProduct::all();
		$cat_type = DB::table('category_products')
	            ->join('product_type', 'category_products.id', '=', 'product_type.cate_id')
	           
	            ->select('category_products.*', 'product_type.type_name')
	            ->get();
	    // if(empty($key)){
	    // 	$product = Product::all()->paginate(8);
	    // }
	    // else if($key == $request->type_name){
	    // 	$product = DB::table('product')
	    // 				->join->on('product_type','product.type_id','=','product_type.id')
	    // 				->where('product_type.type_name' )
	    		
	    // }
		return view('frontend.page.loaisanpham', compact('cat_pro','cat_type','product'));
	}
	public function postSanPham(Request $request){
		$product = DB::table('product')
					->join('category_products','product.cate_id','=','category_products.id')
					->join('product_type','product.type_id','=','product_type.id')
					->where($where)->paginate(8);
		echo "<pre>";print_r($product);die;
	}
	public function getChiTietSanPham($id) {
		$product = Product::find($id);
		return view('frontend.page.chitietsanpham',compact('product'));
	}
	public function getDanhmucbaiviet() {
		$cat_news = CategoryNews::all();
	

		$type_news_km = TypeNews::find('cate_news_id',1);
		$type_news_ct = TypeNews::where('cate_news_id',2);		
					

		
		return view('frontend.page.danhmucbaiviet',compact('cat_news','type_news_km','type_news_ct'));
	}
	public function getChitietbaiviet($id) {
		return view('frontend.page.chitietbaiviet');
	}
	public function getLienhe() {
		return view('frontend.page.lienhe');
	}
	public function getGiacong() {
		return view('frontend.page.dichvugiacong');
	}
	public function getPhanphoi() {
		return view('frontend.page.dichvuphanphoi');
	}
	public function getChienluoc() {
		return view('frontend.page.chienluoc');
	}
	public function getChungnhan() {
		return view('frontend.page.chungnhan');
	}
	public function getThongdiep() {
		return view('frontend.page.thongdiep');
	}
}
