<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeNews extends Model
{
    protected $table = "type_news";
    public function CategoryNews(){
    	return $this->belongsTo('App\CategoryNews','cate_news_id','id');
    }
    public function News() {
		return $this->hasMany('App\News', 'type_news_id', 'id');
	}
}
