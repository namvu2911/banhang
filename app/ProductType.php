<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model {
	protected $table = "product_type";
	public function cateProduct() {
		return $this->belongsTo('App\CategoryProduct', 'cate_id', 'id');
	}
	public function Product() {
		return $this->hasMany('App\Product', 'type_id', 'id');

	}
}
