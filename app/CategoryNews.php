<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryNews extends Model {
	protected $table = "category_news";
	public function news() {
		return $this->hasMany('App\CategoryNews', 'cat_news_id', 'id');
	}
	public function CategoryNews(){
    	return $this->hasMany('App\TypeNews','cate_news_id','id');
    }
}
