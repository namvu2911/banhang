<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {
	protected $table = "product";
	public function categoryProduct() {
		return $this->belongsTo('App\CategoryProduct', 'cate_id', 'id');

	}
	public function typeProduct() {
		return $this->belongsTo('App\ProductType', 'type_id', 'id');

	}
}
