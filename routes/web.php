<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('admin/dangnhap','UserController@getLogin');
Route::post('admin/dangnhap','UserController@postLogin');
Route::get('admin/dangxuat','UserController@getLogout');

Route::group(['prefix' => 'admin' , 'middleware' => 'checklogin'], function () {
	Route::get('/', 'BannerController@getList');
	Route::group(['prefix' => 'banner'], function () {
		Route::get('list', 'BannerController@getList');
		Route::get('add', 'BannerController@getAdd');
		Route::post('add', 'BannerController@postAdd');
		Route::get('edit/{id}', 'BannerController@getEdit');
		Route::post('edit/{id}', 'BannerController@postEdit');
		Route::get('delete/{id}', 'BannerController@getDel');
	});
	Route::group(['prefix' => 'product'], function () {
		Route::get('list', 'ProductController@getList');
		Route::get('add', 'ProductController@getAdd');
		Route::post('add', 'ProductController@postAdd');
		Route::get('edit/{id}', 'ProductController@getEdit');
		Route::post('edit/{id}', 'ProductController@postEdit');
		Route::get('delete/{id}', 'ProductController@getDel');
	});
	Route::group(['prefix' => 'product_type'], function () {
		Route::get('list', 'ProductTypeController@getList');
		Route::get('add', 'ProductTypeController@getAdd');
		Route::post('add', 'ProductTypeController@postAdd');
		Route::get('edit/{id}', 'ProductTypeController@getEdit');
		Route::post('edit/{id}', 'ProductTypeController@postEdit');
		Route::get('delete/{id}', 'ProductTypeController@getDel');
	});
	Route::group(['prefix' => 'categoryproduct'], function () {
		Route::get('list', 'CategoryProductController@getList');
		Route::get('add', 'CategoryProductController@getAdd');
		Route::post('add', 'CategoryProductController@postAdd');
		Route::get('edit/{id}', 'CategoryProductController@getEdit');
		Route::post('edit/{id}', 'CategoryProductController@postEdit');
		Route::get('delete/{id}', 'CategoryProductController@getDel');
	});
	Route::group(['prefix' => 'category_news'], function () {
		Route::get('list', 'CategoryNewsController@getList');
		Route::get('add', 'CategoryNewsController@getAdd');
		Route::post('add', 'CategoryNewsController@postAdd');
		Route::get('edit/{id}', 'CategoryNewsController@getEdit');
		Route::post('edit/{id}', 'CategoryNewsController@postEdit');
		Route::get('delete/{id}', 'CategoryNewsController@getDel');
	});
	Route::group(['prefix' => 'type_news'], function () {
		Route::get('list', 'TypeNewsController@getList');
		Route::get('add', 'TypeNewsController@getAdd');
		Route::post('add', 'TypeNewsController@postAdd');
		Route::get('edit/{id}', 'TypeNewsController@getEdit');
		Route::post('edit/{id}', 'TypeNewsController@postEdit');
		Route::get('delete/{id}', 'TypeNewsController@getDel');
	});
	Route::group(['prefix' => 'news'], function () {
		Route::get('list', 'NewsController@getList');
		Route::get('add', 'NewsController@getAdd');
		Route::post('add', 'NewsController@postAdd');
		Route::get('edit/{id}', 'NewsController@getEdit');
		Route::post('edit/{id}', 'NewsController@postEdit');
		Route::get('delete/{id}', 'NewsController@getDel');
	});
	Route::group(['prefix' => 'users'], function () {
		Route::get('list', 'UserController@getList');
		Route::get('add', 'UserController@getAdd');
		Route::post('add', 'UserController@postAdd');
		Route::get('edit/{id}', 'UserController@getEdit');
		Route::post('edit/{id}', 'UserController@postEdit');
		Route::get('delete/{id}', 'UserController@getDel');
	});
});

Route::get('/', [
	'as' => 'trang-chu',
	'uses' => 'PageController@getIndex',
]);
Route::get('loaisanpham', [
	'as' => 'loai-san-pham',
	'uses' => 'PageController@getLoaiSanPham',
]);
Route::get('chitietsanpham/{id}', [
	'as' => 'chi-tiet-san-pham',
	'uses' => 'PageController@getChiTietSanPham',
]);
Route::get('danhmucbaiviet', [
	'as' => 'tin-tuc',
	'uses' => 'PageController@getDanhmucbaiviet',
]);
Route::get('chitietbaiviet', [
	'as' => 'chi-tiet',
	'uses' => 'PageController@getChitietbaiviet',
]);
Route::get('lienhe', [
	'as' => 'lien-he',
	'uses' => 'PageController@getLienhe',
]);
Route::get('dichvugiacong', [
	'as' => 'dich-vu-gia-cong',
	'uses' => 'PageController@getGiacong',
]);
Route::get('dichvuphanphoi', [
	'as' => 'dich-vu-phan-phoi',
	'uses' => 'PageController@getPhanphoi',
]);
Route::get('chienluoc', [
	'as' => 'chien-luoc',
	'uses' => 'PageController@getChienluoc',
]);
Route::get('chungnhan', [
	'as' => 'chung-nhan',
	'uses' => 'PageController@getChungnhan',
]);
Route::get('thongdiep', [
	'as' => 'thong-diep',
	'uses' => 'PageController@getThongdiep',
]);

