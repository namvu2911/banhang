<footer class="footer" id="footer">
                  <div class="hidden-xs hidden-sm" style="position: absolute; left: 100px; bottom: 150px">
                     <script language="JavaScript" src="https://dunsregistered.dnb.com/" type="text/javascript"></script>
                  </div>
                  <div class="container">
                     <div class="socials">
                        <ul>
                           <li>
                              <a href="https://www.facebook.com/Cophandaidongtien"><i class="fa fa-facebook"></i></a>
                           </li>
                           <li>
                              <a href="https://www.linkedin.com/company/1610451"><i class="fa fa-linkedin"></i></a>
                           </li>
                           <li>
                              <a href="https://plus.google.com/u/0/101469469759494403152"><i class="fa fa-google-plus"></i></a>
                           </li>
                           <li>
                              <a href="https://www.youtube.com/channel/UCelX5PrRAvXp_kwSBDYIcrw"><i class="fa fa-youtube"></i></a>
                           </li>
                           <li>
                              <a href="https://www.instagram.com/daidongtien"><i class="fa fa-instagram"></i></a>
                           </li>
                        </ul>
                     </div>
                     <div class="intro-text">
                        <span>
                        <a href="{{route('dich-vu-phan-phoi')}}">Hệ thống phân phối</a>
                        </span>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                        <a href="http://talent.daidongtien.com.vn/">Tuyển dụng</a>
                     </div>
                     <address class="address">
                        <i class="fa fa-map-marker"></i>
                        Địa chỉ: 216 Tân Thành, Phường 15, Quận 5, Tp. HCM
                        &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                        <i class="fa fa-phone"></i>
                        Tel: +84 906 600 183
                        &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                        <i class="fa fa-user"></i>
                        Hotline: 1800 588 893
                     </address>
                  </div>
                  <div class="copyright">Copyright &copy; 2018 Đại Đồng Tiến</div>
               </footer>