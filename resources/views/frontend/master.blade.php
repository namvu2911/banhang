<!DOCTYPE html>

    <html lang="en">

         <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
         <!-- /Added by HTTrack -->
        @include('frontend.head')
         <body class="homepage page ">
            <!-- Google Tag Manager (noscript) -->
            <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5ZPN7QD"
               height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
            <!-- End Google Tag Manager (noscript) -->
            <input type="checkbox" class="hidden" id="menu_visible_trigger">
            <div class="wrapper" id="site_wrapper">
              @include('frontend.header')
               @yield('content')
              @include('frontend.footer')
               <a href="javascript:;" class="go-top"><i class="fa fa-angle-up"></i></a>
            </div>
            <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
            <![endif]-->
            <script src="assets/themes/dai-dong-tien/third_party/compiled.js"></script>
            <script src="assets/themes/dai-dong-tien/dist/app.js"></script>
            <!--Start of Zendesk Chat Script-->
            <script type="text/javascript">
               window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
               d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
               _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
               $.src="https://v2.zopim.com/?3jral68w1dWAHNGUN8tLGD17dVuBTCo4";z.t=+new Date;$.
               type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
            </script>
            <!--End of Zendesk Chat Script-->
            <div class="modal fade" tabindex="-1" role="dialog" id="error_messages_modal">
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title">Dai Dong Tien</h4>
                     </div>
                     <div class="modal-body">
                     </div>
                  </div>
               </div>
            </div>
            <script src="assets/admin/plugins/jquery-waypoints/jquery.waypoints.min.js"></script>
            <script src="assets/themes/dai-dong-tien/third_party/jquery.animateNumber.min.js"></script>
         </body>
         <!-- Mirrored from www.daidongtien.com.vn/vi/trang-chu by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Apr 2018 01:48:53 GMT -->
      </html>