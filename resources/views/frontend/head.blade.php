<head>
            <!-- Google Tag Manager -->
            <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
               new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
               j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
               '../../www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);
               })(window,document,'script','dataLayer','GTM-5ZPN7QD');
            </script>
            <!-- End Google Tag Manager -->
            <title>Trang chủ - Đại Đồng Tiến</title>
             <base href="{{asset('')}}">
            <meta name="csrf-token" content="1k5vUJeQY9f7j0H5ABQuhGodNh1IK9Z72G5pK2Eb">
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
            <link rel="canonical" href="../index.html">
            <meta http-equiv="content-language" content="vi">
            <link rel="shortcut icon" href="../uploads/1/logo-congty.png">
            <meta name="description" content="">
            <meta name="keywords" content="nhựa gia dụng,nhựa chất lượng cao, gia công sản phẩm nhựa,sản phẩm nhựa">
            <meta content="" name="author">
            <!-- Google+ -->
            <meta itemprop="name" content="Trang chủ - Đại Đồng Tiến">
            <meta itemprop="description" content="">
            <meta itemprop="keywords" content="nhựa gia dụng,nhựa chất lượng cao, gia công sản phẩm nhựa,sản phẩm nhựa">
            <meta itemprop="image" content="https://www.daidongtien.com.vn/uploads/1/brands/site-logo.svg">
            <!-- Twitter Card data -->
            <meta name="twitter:card" content="https://www.daidongtien.com.vn/uploads/1/brands/site-logo.svg">
            <meta name="twitter:site" content="https://www.daidongtien.com.vn/vi/trang-chu">
            <meta name="twitter:title" content="Trang chủ - Đại Đồng Tiến">
            <meta name="twitter:description" content="">
            <meta name="twitter:creator" content="https://www.daidongtien.com.vn/vi/trang-chu">
            <!-- Twitter summary card with large image must be at least 280x150px -->
            <meta name="twitter:image:src" content="https://www.daidongtien.com.vn/uploads/1/brands/site-logo.svg">
            <!-- Open Graph data -->
            <meta property="og:title" content="Trang chủ - Đại Đồng Tiến">
            <meta property="og:type" content="article">
            <meta property="og:url" content="https://www.daidongtien.com.vn/vi/trang-chu">
            <meta property="og:image" content="https://www.daidongtien.com.vn/uploads/1/brands/site-logo.svg">
            <meta property="og:description" content="">
            <meta property="og:site_name" content="Đại Đồng Tiến">
            <meta property="article:published_time"
               content="2017-07-11 07:11:05">
            <meta property="article:modified_time"
               content="2018-02-28 01:23:39">
            <meta property="article:section" content="Trang chủ - Đại Đồng Tiến">
            <meta property="article:tag" content="nhựa gia dụng,nhựa chất lượng cao, gia công sản phẩm nhựa,sản phẩm nhựa">
            <meta property="fb:admins" content="">
            <script>
               var BASE_URL = '../index.html';
            </script>
            <base >
            <link rel="stylesheet" href="assets/themes/dai-dong-tien/third_party/slick/slick.css">
            <link rel="stylesheet" href="assets/themes/dai-dong-tien/third_party/slick/slick-theme.css">
            <link rel="stylesheet" href="assets/themes/dai-dong-tien/third_party/font-awesome/css/font-awesome.min.css">
            <link rel="stylesheet" href="assets/themes/dai-dong-tien/third_party/malihu-custom-scrollbar/jquery.mCustomScrollbar.css">
            <link rel="stylesheet" href="assets/themes/dai-dong-tien/dist/app.css">
            <link href="assets/themes/dai-dong-tien/fonts/font.css" rel="stylesheet">
        </head>