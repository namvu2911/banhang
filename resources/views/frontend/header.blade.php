 <header class="header" id="header">
                  <div class="container">
                     <div class="logo">
                        <a href="../vi.html">
                        <img src="public/uploads/1/brands/home-logo-vi.svg" alt="">
                        </a>
                     </div>
                     <label for="menu_visible_trigger" class="visible-xs show-menu"><i class="fa fa-bars"></i></label>
                     <div class="menu-wrapper">
                        <label for="menu_visible_trigger" class="visible-xs hide-menu"><i class="glyphicon glyphicon-remove"></i></label>
                        <nav class=""
                           id="">
                           <ul role="menu"
                              id=""
                              class="">
                              <li class="menu-item
                                 active
                                 ">
                                 <a href="{{route('trang-chu')}}"
                                    title="Trang chủ">
                                 Trang chủ
                                 </a>
                              </li>
                              <li class="menu-item
                                 menu-item-has-children dropdown">
                                 <a href="{{route('thong-diep')}}"
                                    title="Về công ty">
                                 Về công ty
                                 </a>
                                 <ul role="menu"
                                    id=""
                                    class="sub-menu">
                                    <li class="menu-item
                                       ">
                                       <a href="{{route('thong-diep')}}"
                                          title="Thông điệp từ CEO">
                                       Thông điệp từ CEO
                                       </a>
                                    </li>
                                    <li class="menu-item
                                       ">
                                       <a href="{{route('chung-nhan')}}"
                                          title="Giấy chứng nhận">
                                       Giấy chứng nhận
                                       </a>
                                    </li>
                                    <li class="menu-item
                                       ">
                                       <a href="{{route('chien-luoc')}}"
                                          title="Chiến lược">
                                       Chiến lược
                                       </a>
                                    </li>
                                 </ul>
                              </li>
                              <li class="menu-item
                                 ">
                                 <a href="{{route('loai-san-pham')}}"
                                    title="Sản phẩm">
                                 Sản phẩm
                                 </a>
                              </li>
                              <li class="menu-item
                                 menu-item-has-children dropdown">
                                 <a href="{{route('dich-vu-phan-phoi')}}"
                                    title="Dịch vụ">
                                 Dịch vụ
                                 </a>
                                 <ul role="menu"
                                    id=""
                                    class="sub-menu">
                                    <li class="menu-item
                                       ">
                                       <a href="{{route('dich-vu-phan-phoi')}}"
                                          title="Dịch vụ Phân phối">
                                       Dịch vụ Phân phối
                                       </a>
                                    </li>
                                    <li class="menu-item
                                       ">
                                       <a href="{{route('dich-vu-gia-cong')}}"
                                          title="Dịch vụ Gia công">
                                       Dịch vụ Gia công
                                       </a>
                                    </li>
                                 </ul>
                              </li>
                              <li class="menu-item
                                 ">
                                 <a href="{{route('tin-tuc')}}"
                                    title="Tin tức">
                                 Tin tức
                                 </a>
                              </li>
                              <li class="menu-item
                                 ">
                                 <a href="{{route('lien-he')}}"
                                    title="Liên hệ">
                                 Liên hệ
                                 </a>
                              </li>
                           </ul>
                        </nav>
                        <div class="languages-switcher languages-switcher-list">
                           <ul>
                              <li>
                                 <a href="" title="English">
                                 <span class="flag">
                                 <img src="public/plugins/multilingual/flags/en.png"
                                    class="img-responsive"
                                    alt="English">
                                 </span>
                                 <span class="text">English</span>
                                 </a>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </header>