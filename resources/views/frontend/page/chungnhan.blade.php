@extends('frontend.master')
@section('content')
  <main class="main" id="main">
                  <section class="page-section page-title">
                     <div class="container">
                        <h1>Giấy chứng nhận</h1>
                        <ul class="breadcrumb">
                           <li><a title="Trang chủ" href="../vi.html">Trang chủ</a></li>
                           <li><span>Giấy chứng nhận</span></li>
                        </ul>
                     </div>
                  </section>
                  <section class="page-section main-section">
                     <div class="page-banner">
                        <img src="public/uploads/1/page-templates/about-us/certificate/certificates-banner.png"
                           alt="Giấy chứng nhận"
                           class="img-responsive">
                     </div>
                     <div class="main-content">
                        <div class="container">
                           <div class="about-us-certificates-content">
                              <div class="description"></div>
                              <ul class="certificates-list">
                                 <li>
                                    <div class="wrapper">
                                       <div class="image">
                                          <img src="public/uploads/1/page-templates/about-us/certificate/certificates-iso9001-2008-6.png"
                                             alt="ISO 9001:2008"
                                             class="img-responsive">
                                       </div>
                                       <div class="h3">
                                          ISO 9001:2008
                                       </div>
                                       <div class="certificate-description">
                                          Ngày 22/04/2014 Đại Đồng Tiến được cấp chứng chỉ ISO 9001:2008 là tiêu chuẩn về hệ thống quản lý chất lượng. ISO 9001: 2008 thể hiện sự cam kết của Đại Đồng Tiến đối với khách hàng, hoàn thiện và nâng cao chất lượng sản phẩm.
                                       </div>
                                    </div>
                                 </li>
                                 <li>
                                    <div class="wrapper">
                                       <div class="image">
                                          <img src="public/uploads/1/page-templates/about-us/certificate/certificates-iso-14001-2015.png"
                                             alt="ISO 14001 : 2015"
                                             class="img-responsive">
                                       </div>
                                       <div class="h3">
                                          ISO 14001 : 2015
                                       </div>
                                       <div class="certificate-description">
                                          Vào ngày 29/06/2016 Đại Đồng Tiến được cấp chứng nhận ISO 14001:2015 là tiêu chuẩn về hệ thống quản lý môi trường. Đây là minh chứng cho sự cam kết của Đại Đồng Tiến đối với khách hàng,  luôn hoàn thiện và nâng cao chất lượng, bảo vệ môi trường.
                                       </div>
                                    </div>
                                 </li>
                                 <li>
                                    <div class="wrapper">
                                       <div class="image">
                                          <img src="public/uploads/1/page-templates/about-us/certificate/certificates-bsci-4.png"
                                             alt="BSCI REPORT"
                                             class="img-responsive">
                                       </div>
                                       <div class="h3">
                                          BSCI REPORT
                                       </div>
                                       <div class="certificate-description">
                                          Đại Đồng Tiến rất vinh dự là công ty nhựa đầu tiên Việt Nam đước cấp chứng nhận đạt tiêu chuẩn BSCI là bộ tiêu chuẩn đánh giá tuân thủ trách nhiệm xã hội trong sản xuất kinh doanh, ra đời năm 2003 từ đề xướng của Hiệp hội Ngoại thương (FTA).
                                       </div>
                                    </div>
                                 </li>
                                 <li>
                                    <div class="wrapper">
                                       <div class="image">
                                          <img src="public/uploads/1/page-templates/about-us/certificate/certificates-fda.png"
                                             alt="FDA CERTIFICATION"
                                             class="img-responsive">
                                       </div>
                                       <div class="h3">
                                          FDA CERTIFICATION
                                       </div>
                                       <div class="certificate-description">
                                          FDA là tiêu chuẩn yêu cầu về an toàn thực phẩm và dược phẩm đối với các hàng hóa lưu thông trên thị trường Mỹ. Đại Đồng Tiến luôn đặt mục tiêu là mang đến cho mọi khách hàng những sản phẩm tốt cho sức khoẻ của bạn và gia đình.
                                       </div>
                                    </div>
                                 </li>
                                 <li>
                                    <div class="wrapper">
                                       <div class="image">
                                          <img src="public/uploads/1/page-templates/about-us/certificate/certificates-icti.png"
                                             alt="ICTI CARE CERTIFICATION"
                                             class="img-responsive">
                                       </div>
                                       <div class="h3">
                                          ICTI CARE CERTIFICATION
                                       </div>
                                       <div class="certificate-description">
                                          Vào ngày 29/01/2015 Đại Đồng Tiến rất vinh dự được cấp chứng nhận quốc tế ICTI Seal A là cam kết rõ ràng nhất và mạnh mẽ nhất của chúng tôi với khách hàng của mình về sản phẩm đồ chơi trẻ em của công ty nói riêng và các sản phẩm khác nói chung.
                                       </div>
                                    </div>
                                 </li>
                                 <li>
                                    <div class="wrapper">
                                       <div class="image">
                                          <img src="public/uploads/1/page-templates/about-us/certificate/certificates-meta.png"
                                             alt="SMETA 4 PILLARS STANDARD"
                                             class="img-responsive">
                                       </div>
                                       <div class="h3">
                                          SMETA 4 PILLARS STANDARD
                                       </div>
                                       <div class="certificate-description">
                                          SMETA là một phương pháp đánh giá & báo cáo về thực hành đạo đức và trách nhiệm xã hội được công nhận và sử dụng rộng rãi. Ngày 09/03/2015 Đại Đồng Tiến vinh dự được cấp chứng nhận SMETA 4 PILLARS STANDARD..
                                       </div>
                                    </div>
                                 </li>
                                 <li>
                                    <div class="wrapper">
                                       <div class="image">
                                          <img src="public/uploads/1/page-templates/about-us/certificate/certificates-lfgb.png"
                                             alt="LFGB CERTIFICATION"
                                             class="img-responsive">
                                       </div>
                                       <div class="h3">
                                          LFGB CERTIFICATION
                                       </div>
                                       <div class="certificate-description">
                                          Sản phẩm của Đại Đồng Tiến luôn hướng đến những sản phẩm đạt chuẩn quốc tế, an toàn cho sức khoẻ. Với việc đat được chứng chỉ LFGB (quy định của Đức về việc quản lý an toàn thực phẩm) nhằm khẳng định mạnh mẽ những sản phẩm của chúng tôi.
                                       </div>
                                    </div>
                                 </li>
                                 <li>
                                    <div class="wrapper">
                                       <div class="image">
                                          <img src="public/uploads/1/page-templates/about-us/certificate/certificates-vnr.png"
                                             alt="VIET NAM’S TOP 500 LARGEST ENTERPRISES"
                                             class="img-responsive">
                                       </div>
                                       <div class="h3">
                                          VIET NAM’S TOP 500 LARGEST ENTERPRISES
                                       </div>
                                       <div class="certificate-description">
                                          Đại Đồng Tiến rất vinh dự là doanh nghiệp lớn thứ 350 của Việt Nam trong năm 2016 theo VNR500 dựa trên kết quả nghiên cứu và đánh giá độc lập theo chuẩn mực quốc tế của Công ty Vietnam Report với sự tư vấn của các chuyên gia trong và ngoài nước.
                                       </div>
                                    </div>
                                 </li>
                                 <li>
                                    <div class="wrapper">
                                       <div class="image">
                                          <img src="public/uploads/1/page-templates/about-us/certificate/certificates-cr.png"
                                             alt="Chứng Nhận Hợp Qui CR"
                                             class="img-responsive">
                                       </div>
                                       <div class="h3">
                                          Chứng Nhận Hợp Qui CR
                                       </div>
                                       <div class="certificate-description">
                                          Đại Đồng Tiến được cấp chứng nhận hợp quy CR để đảm bảo rằng những sản phẩm, hàng hóa của chúng tôi hoàn toàn đáp ứng các yêu cầu của quy chuẩn kỹ thuật quốc gia liên quan và được phép lưu thông trên thị trường.
                                       </div>
                                    </div>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </section>
               </main>
@endsection