@extends('frontend.master')
@section('content')
<main class="main" id="main">
                  <section class="page-section page-title">
                     <div class="container">
                        <h1>Đại Đồng Tiến Giao Lưu Kết Nối CLB Doanh Nhân Sài Gòn</h1>
                        <ul class="breadcrumb"></ul>
                     </div>
                  </section>
                  <section class="page-section main-section columns2">
                     <div class="container">
                        <div class="main-content">
                           <div class="post-content">
                              <p style="text-align: justify;"><span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">TP.&nbsp;Hồ Ch&iacute; Minh 04/07/2015&nbsp;&nbsp;Nằm trong mục ti&ecirc;u kinh doanh, c&acirc;u hỏi của Doanh nh&acirc;n S&agrave;i G&ograve;n với đại diện 35 doanh nghiệp đ&atilde; đến thăm C&ocirc;ng ty Cổ phần Đại Đồng, doanh nghiệp lớn Trong lĩnh vực c&ocirc;ng nghệ nhựa tại TP.&nbsp;Hồ Ch&iacute; Minh.&nbsp;Đo&agrave;n doanh nh&acirc;n đ&atilde; đến tham quan nh&agrave; m&aacute;y sản xuất của Đại Đồng Tiến tại quận B&igrave;nh T&acirc;n, thăm c&aacute;c ph&acirc;n xưởng v&agrave; d&acirc;y chuyền, hoạt động 24/24 để đảm bảo cho c&aacute;c đơn h&agrave;ng v&agrave; kế hoăch ph&aacute;t triển m&agrave; Đại Đồng Tiến đ&atilde; đi v&agrave;o quy tr&igrave;nh từ nhiều năm nay.</span></span></span></p>
                              <p style="text-align: center;"><span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;"><img alt="" src="../../ddt.piksal.in/uploads/homepage/Tin%20t%e1%bb%a9c%201/img_9648.jpg" style="width: 1181px; height: 891px;" /></span></span></span></p>
                              <p style="text-align: center;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">C&acirc;u lạc bộ Doanh Nh&acirc;n S&agrave;i G&ograve;n chụp ảnh lưu niệm</span></span></p>
                              <p style="text-align: justify;"><span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">&Ocirc;ng Trịnh Ch&iacute; Cường, Tổng gi&aacute;m đốc c&ocirc;ng ty Đại Đồng Tiến n&oacute;i: &ldquo;Kinh doanh hiện nay kh&ocirc;ng chỉ nhằm mục ti&ecirc;u lợi nhuận, v&igrave; thế t&ocirc;i rất vui mừng khi thấy C&acirc;u lạc bộ Doanh nh&acirc;n S&agrave;i G&ograve;n đ&atilde; bỏ một ng&agrave;y l&agrave;m việc để đến thăm Đại Đồng Tiến v&igrave; những mục ti&ecirc;u mang t&iacute;nh văn h&oacute;a x&atilde; hội bền vững hơn cho c&aacute;c doanh nghiệp&rdquo;.</span></span></span></p>
                              <p style="text-align: justify;"><span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;"><img alt="" src="../Data/Sites/1/userfiles/9/img_9432.html" style="width: 1181px; height: 787px;" /></span></span></span></p>
                              <p style="text-align: center;"><span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">&Ocirc;ng Trịnh Ch&iacute; Cường- TGĐ Đại Đồng Tiến chia sẻ tại chương tr&igrave;nh</span></span></span></p>
                              <p style="text-align: justify;"><span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">Tổng gi&aacute;m đốc Đại Đồng Tiến cũng chia sẻ c&aacute;ch x&acirc;y dựng v&agrave; điều h&agrave;nh một c&ocirc;ng ty với hai nh&agrave; m&aacute;y v&agrave; hơn 1000 c&aacute;n bộ c&ocirc;ng nh&acirc;n vi&ecirc;n tại Đại Đồng Tiến cũng như một số định hướng ph&aacute;t triển của c&ocirc;ng ty trong c&aacute;c năm tới. Trong chuyến thăm, c&aacute;c doanh nh&acirc;n trong C&acirc;u lạc bộ Doanh Nh&acirc;n S&agrave;i G&ograve;n c&ugrave;ng với chủ nhiệm CLB Nguyễn Thanh Minh đ&atilde; xem v&agrave; ghi nhận c&aacute;ch ph&aacute;t triển n&acirc;ng cấp nh&agrave; m&aacute;y l&agrave;m sao để ph&ugrave; hợp với quy m&ocirc; c&ocirc;ng ty ng&agrave;y c&agrave;ng mở rộng m&agrave; vẫn duy tr&igrave; được t&iacute;nh kế thừa.</span></span></span></p>
                              <p style="text-align: justify;"><span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">Trong khu&ocirc;n khổ một chuyến thăm chỉ k&eacute;o d&agrave;i hơn 3 tiếng đồng hồ, đo&agrave;n Doanh nh&acirc;n C&acirc;u lạc bộ Doanh Nh&acirc;n S&agrave;i G&ograve;n đ&atilde; chia sẻ được nhiều vấn đề hữu &iacute;ch, hiểu th&ecirc;m về đặc th&ugrave; của từng m&ocirc; h&igrave;nh doanh nghiệp, đồng thời tạo ra sự khởi đầu cho việc kết nối khi C&acirc;u lạc bộ Doanh nh&acirc;n S&agrave;i G&ograve;n đang ng&agrave;y c&agrave;ng ph&aacute;t triển v&agrave; mở rộng.</span></span></span></p>
                              <p style="text-align: center;"><span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;"><img alt="" src="../../daidongtien.tk/uploads/2/tin-cong-ty/img-9519-1.jpg" style="width: 1181px; height: 788px;" /><img alt="" src="../../ddt.piksal.in/uploads/homepage/Tin%20t%e1%bb%a9c%201/img_9519.jpg" style="width: 1181px; height: 788px;" /></span></span></span></p>
                              <p style="text-align: center;"><span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">C&aacute;c doanh nh&acirc;n c&ugrave;ng tham quan v&agrave; t&igrave;m hiểu quy tr&igrave;nh sản xuất tại Nh&agrave; m&aacute;y</span></span></span></p>
                              <p style="text-align: justify;"><span style="color:#000000;"><span style="font-size:14px;"><span style="font-family:arial,helvetica,sans-serif;">Với tin nhắn &quot;&nbsp;Kết nối Kinh doanh - L&agrave;m việc&nbsp;&quot; giao lưu, học hỏi v&agrave; kết nối kinh doanh được tổ chức bởi Ban Kết Nối Kinh Doanh - Nh&oacute;m Doanh nh&acirc;n S&agrave;i g&ograve;n sẽ bắt đầu từ th&aacute;ng 7 năm 2015 V&agrave; ban đầu C&ocirc;ng ty Đại Đồng Tiến&nbsp;Theo &Ocirc;ng Phan C&ocirc;ng Ch&iacute;nh - Trưởng Ban Kết Nối - Chuỗi k&eacute;o d&agrave;i trong 12 th&aacute;ng, mỗi th&aacute;ng C&acirc;u hỏi sẽ l&agrave; một Gi&aacute;m s&aacute;t Doanh nghiệp v&agrave; Tổ chức c&aacute;c hoạt động kinh doanh, hỗ trợ sự hợp t&aacute;c giữa c&aacute;c Business in the club Doanh nghiệp S&agrave;i G&ograve;n.</span></span></span></p>
                              <p style="text-align: justify;">&nbsp;</p>
                           </div>
                           <div class="text-center other-content-group">
                              <div class="other-data">
                                 <ul>
                                    <li>Danh mục:
                                       <a href="tin-khuyen-mai.html"
                                          title="Tin Khuyến Mãi">Tin Khuyến Mãi
                                       </a>
                                    </li>
                                    <li>04/07/2015</li>
                                 </ul>
                              </div>
                              <div class="post-tags">
                                 <ul>
                                 </ul>
                              </div>
                              <div class="share-post">
                                 <div class="h3">Chia sẻ bài đăng</div>
                                 <div class="socials">
                                    <ul>
                                       <li class="twitter">
                                          <a href="https://twitter.com/home?status=https://www.daidongtien.com.vn/vi/dai-dong-tien-giao-luu-ket-noi-clb-doanh-nhan-sai-gon.html">
                                          <i class="fa fa-twitter"></i>
                                          </a>
                                       </li>
                                       <li class="facebook">
                                          <a href="https://www.facebook.com/sharer/sharer.php?u=https://www.daidongtien.com.vn/vi/dai-dong-tien-giao-luu-ket-noi-clb-doanh-nhan-sai-gon.html">
                                          <i class="fa fa-facebook"></i>
                                          </a>
                                       </li>
                                       <li class="instagram">
                                          <a href="https://plus.google.com/share?url=https://www.daidongtien.com.vn/vi/dai-dong-tien-giao-luu-ket-noi-clb-doanh-nhan-sai-gon.html">
                                          <i class="fa fa-google-plus"></i>
                                          </a>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="comments">
                                 <div class="h2">Bình luận</div>
                                 <div class="social-comments">
                                    <div class="facebook-comment">
                                       <div class="fb-comments"
                                          data-href=""
                                          data-numposts="15"></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <aside class="sidebar">
                           <form action="https://www.daidongtien.com.vn/vi/tin-tuc.html" method="GET" class="search-form">
                              <div class="input-group">
                                 <input type="text"
                                    class="form-control"
                                    placeholder="Tìm kiếm..."
                                    value=""
                                    name="k">
                                 <div class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                    <i class="glyphicon glyphicon-search"></i>
                                    </button>
                                 </div>
                              </div>
                           </form>
                           <div class="well">
                              <div class="h4">Danh mục</div>
                              <div class="well-content">
                                 <ul class="list-unstyled categories-list">
                                    <li>
                                       <a href="tin-tuc8b4e.html?tab=dai-dong-tien-news">
                                       Tin Công Ty (28)
                                       </a>
                                    </li>
                                    <li>
                                       <a href="tin-tucb669.html?tab=tin-khuyen-mai">
                                       Tin Khuyến Mãi (6)
                                       </a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                           <div class="well">
                              <div class="h4">Tags</div>
                              <div class="well-content">
                                 <ul class="list-unstyled tags-list">
                                    <li>
                                       <a href="post-tag/nha-san-xuat-nhua-so-1.html" title="Nhà Sản Xuất Nhựa Số 1">
                                       Nhà Sản Xuất Nhựa Số 1
                                       </a>
                                    </li>
                                    <li>
                                       <a href="post-tag/dai-dong-tien.html" title="Dai Dong Tien">
                                       Dai Dong Tien
                                       </a>
                                    </li>
                                    <li>
                                       <a href="post-tag/finance.html" title="Finance">
                                       Finance
                                       </a>
                                    </li>
                                    <li>
                                       <a href="post-tag/pp.html" title="PP">
                                       PP
                                       </a>
                                    </li>
                                    <li>
                                       <a href="post-tag/world.html" title="World">
                                       World
                                       </a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                           <div class="well">
                              <div class="h4">Bài viết gần đây</div>
                              <div class="well-content">
                                 <ul class="list-unstyled recent-posts">
                                    <li>
                                       <div class="media">
                                          <div class="media-left">
                                             <a href="chuong-trinh-mo-mat-mien-phi-cho-benh-nhan-ngheo-tai-ho-chi-minh.html" title="Chương Trình Mổ Mắt Miễn Phí Cho Bệnh Nhân Nghèo Tại Hồ Chí Minh">
                                             <img class="media-object"
                                                width="62"
                                                src="../uploads/5/tin-cong-ty/dai-dong-tien-tai-tro-chuong-trinh-mo-mat-tu-thien-cho-nguoi-ngheo-3-150xauto.jpg"
                                                alt="Chương Trình Mổ Mắt Miễn Phí Cho Bệnh Nhân Nghèo Tại Hồ Chí Minh">
                                             </a>
                                          </div>
                                          <div class="media-body">
                                             <h4 class="media-heading">
                                                <a href="chuong-trinh-mo-mat-mien-phi-cho-benh-nhan-ngheo-tai-ho-chi-minh.html" title="Chương Trình Mổ Mắt Miễn Phí Cho Bệnh Nhân Nghèo Tại Hồ Chí Minh">
                                                Chương Trình Mổ Mắt Miễn Phí Cho Bệnh Nhân Nghèo Tại Hồ Chí Minh
                                                </a>
                                             </h4>
                                             <div class="date-time">03/03/2018</div>
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="media">
                                          <div class="media-left">
                                             <a href="chuong-trinh-to-chuc-cho-hoa-xuan-tai-quan-5-nham-phuc-vu-tet-cho-nguoi-dan.html" title="Chương Trình Tổ Chức Chợ Hoa Xuân Tại Quận 5 Nhằm Phục Vụ Tết Cho Người Dân">
                                             <img class="media-object"
                                                width="62"
                                                src="../uploads/5/tin-cong-ty/hoi-tro-hoa-tet-tai-quan-5-hcm-150xauto.jpg"
                                                alt="Chương Trình Tổ Chức Chợ Hoa Xuân Tại Quận 5 Nhằm Phục Vụ Tết Cho Người Dân">
                                             </a>
                                          </div>
                                          <div class="media-body">
                                             <h4 class="media-heading">
                                                <a href="chuong-trinh-to-chuc-cho-hoa-xuan-tai-quan-5-nham-phuc-vu-tet-cho-nguoi-dan.html" title="Chương Trình Tổ Chức Chợ Hoa Xuân Tại Quận 5 Nhằm Phục Vụ Tết Cho Người Dân">
                                                Chương Trình Tổ Chức Chợ Hoa Xuân Tại Quận 5 Nhằm Phục Vụ Tết Cho Người Dân
                                                </a>
                                             </h4>
                                             <div class="date-time">09/02/2018</div>
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="media">
                                          <div class="media-left">
                                             <a href="chuong-trinh-mo-tim-tu-thien-cho-be-thien-manh-tai-huyen-binh-chanh-ho-chi-minh.html" title="Chương Trình Mổ Tim Từ Thiện Cho Bé Thiên Mạnh Tại Huyện Bình Chánh Hồ Chí Minh">
                                             <img class="media-object"
                                                width="62"
                                                src="../uploads/5/tin-cong-ty/mo-tim-tu-thien-1-1-150xauto.jpg"
                                                alt="Chương Trình Mổ Tim Từ Thiện Cho Bé Thiên Mạnh Tại Huyện Bình Chánh Hồ Chí Minh">
                                             </a>
                                          </div>
                                          <div class="media-body">
                                             <h4 class="media-heading">
                                                <a href="chuong-trinh-mo-tim-tu-thien-cho-be-thien-manh-tai-huyen-binh-chanh-ho-chi-minh.html" title="Chương Trình Mổ Tim Từ Thiện Cho Bé Thiên Mạnh Tại Huyện Bình Chánh Hồ Chí Minh">
                                                Chương Trình Mổ Tim Từ Thiện Cho Bé Thiên Mạnh Tại Huyện Bình Chánh Hồ Chí Minh
                                                </a>
                                             </h4>
                                             <div class="date-time">08/01/2018</div>
                                          </div>
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </aside>
                     </div>
                  </section>
               </main>
@endsection