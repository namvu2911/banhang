@extends('frontend.master')
@section('content')
<main class="main" id="main">
   <section class="page-section page-title">
      <div class="container">
         <h1></h1>
         <ul class="breadcrumb">
            <li><a title="Trang chủ" href="{{route('trang-chu')}}">Trang chủ</a></li>
           
         </ul>
      </div>
   </section>
   <section class="page-section main-section">
      <div class="page-banner">
         <img src="../uploads/2/banner-group-product/ghe.jpg"
            alt="Thùng Rác"
            class="img-responsive hidden-xs">
         <img src="../uploads/2/banner-group-product/ghe-mobile.jpg"
            alt="Thùng Rác"
            class="img-responsive visible-xs">
      </div>
      <div class="main-content">
         <div class="container">
            <div class="title-with-description">
               <h1 class="h1">Sản phẩm</h1>
               <div class="description"></div>
            </div>
            <div class="products-grid">
               <div class="filters">
                  <div class="form-wrapper">

                     <input type="checkbox" id="check_filter_visible_xs" class="hidden">
                     <label for="check_filter_visible_xs" class="hidden">
                     <i class="fa fa-filter"></i>
                     </label>
                     <form role="search" action="search" class="form" method="POST" id ="searchform">
                     <input type="hidden" name="_token" value="{{csrf_token()}}">

                        <span class="text text-uppercase">Tìm kiếm:</span>
                        <a href="" class="btn-special">Tất cả</a>
                        <div class="custom-select select-categories">
                           <select name=""
                              class="form-control change-url-select">
                              <option data-url=""
                                 data-is-parent="true"
                                 value="1">
                                 Sản phẩm
                              </option>
                             
                             @foreach($cat_type as $item)
                              
                              <option data-url="{{$item->id}}"
                                                data-is-parent="true"
                                                value="4">
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$item->name}}
                              </option>
                              @if($item->parent == 1)   
                                 
                              
                              @endif   
                                         
                              @endforeach
                           </select>
                        </div>
                        <div class="custom-select select-categories">
                           <select name="brand"
                              class="form-control change-url-select"
                              data-placeholder="Nhóm sản phẩm">
                              <option value=""></option>
                              @foreach($cat_pro as $item)
                              <option data-url="{{$item->id}}"
                                 value="52">
                                {{$item->name}}
                              </option>
                            @endforeach
                           </select>
                        </div>
                     </form>
                  </div>
               </div>
               <div id="waypoint"></div>
               <ul class="js-mansory-layout">
                  <li class="grid-item">
                     <div class="wrapper">
                        <div class="product-image">
                           <a href="thung-rac-super-bin.html"
                              title="Thùng Rác Super Bin">
                           <img
                              src="../uploads/2/household/sot-rac/thung-rac-superbin-1-300xauto.jpg"
                              alt="Thùng Rác Super Bin"
                              class="img-responsive">
                           </a>
                           <div class="label-wrapper">
                              <div class="product-labels">
                              </div>
                           </div>
                        </div>
                        <a class="product-name" href="thung-rac-super-bin.html">
                        <span>Thùng Rác Super Bin</span>
                        </a>
                        <div class="actions">
                           <div class="add-to-wishlist">
                              <a href="javascript:;"
                                 class=" add-to-liked"
                                 data-url="../product/like/1612.html"><i
                                 class="fa fa-heart-o"></i></a>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li class="grid-item">
                     <div class="wrapper">
                        <div class="product-image">
                           <a href="thung-rac-mr-bin.html"
                              title="Thùng Rác Mr Bin">
                           <img
                              src="../uploads/2/household/sot-rac/thung-rac-mrbin-xanh-la-1-300xauto.jpg"
                              alt="Thùng Rác Mr Bin"
                              class="img-responsive">
                           </a>
                           <div class="label-wrapper">
                              <div class="product-labels">
                              </div>
                           </div>
                        </div>
                        <a class="product-name" href="thung-rac-mr-bin.html">
                        <span>Thùng Rác Mr Bin</span>
                        </a>
                        <div class="actions">
                           <div class="add-to-wishlist">
                              <a href="javascript:;"
                                 class=" add-to-liked"
                                 data-url="../product/like/428.html"><i
                                 class="fa fa-heart-o"></i></a>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li class="grid-item">
                     <div class="wrapper">
                        <div class="product-image">
                           <a href="thung-rac-416.html"
                              title="Thùng rác 416">
                           <img
                              src="../uploads/2/household/sot-rac/thung-rac-416-vang-cam-300xauto.jpg"
                              alt="Thùng rác 416"
                              class="img-responsive">
                           </a>
                           <div class="label-wrapper">
                              <div class="product-labels">
                              </div>
                           </div>
                        </div>
                        <a class="product-name" href="thung-rac-416.html">
                        <span>Thùng rác 416</span>
                        </a>
                        <div class="actions">
                           <div class="add-to-wishlist">
                              <a href="javascript:;"
                                 class=" add-to-liked"
                                 data-url="../product/like/416.html"><i
                                 class="fa fa-heart-o"></i></a>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li class="grid-item">
                     <div class="wrapper">
                        <div class="product-image">
                           <a href="thung-rac-trung.html"
                              title="Thùng Rác Trung">
                           <img
                              src="../uploads/2/household/sot-rac/thung-rac-trung-3-300xauto.jpg"
                              alt="Thùng Rác Trung"
                              class="img-responsive">
                           </a>
                           <div class="label-wrapper">
                              <div class="product-labels">
                              </div>
                           </div>
                        </div>
                        <a class="product-name" href="thung-rac-trung.html">
                        <span>Thùng Rác Trung</span>
                        </a>
                        <div class="actions">
                           <div class="add-to-wishlist">
                              <a href="javascript:;"
                                 class=" add-to-liked"
                                 data-url="../product/like/417.html"><i
                                 class="fa fa-heart-o"></i></a>
                           </div>
                        </div>
                     </div>
                  </li>
               </ul>
            </div>
         </div>
      </div>
   </section>
</main>
@endsection