@extends('frontend.master')
@section('content')
 <main class="main" id="main">
                  <section class="page-section page-title">
                     <div class="container">
                        <h1></h1>
                        <ul class="breadcrumb">
                           <li><a title="Trang chủ" href="../vi.html">Trang chủ</a></li>
                           <li><a title="Thùng Nhựa" href="thung-nhua.html">Thùng Nhựa</a></li>
                           <li><span>Thùng Đa Năng 25L</span></li>
                        </ul>
                     </div>
                  </section>
                  <section class="page-section main-section">
                     <div class="main-content">
                        <div class="container">
                           <div class="product-details-wrapper">
                              <div class="product-images-container">
                                 <div class="product-images" data-product-images>
                                    <div class="wrapper js-product-images">
                                       <div class="item">
                                          <div class="image-wrapper">
                                             <div class="fixed-ratio">
                                                <img src="../uploads/7/household/thung/thung-nhua-da-nang-25l-2-90xauto.jpg"
                                                   data-full="../uploads/7/household/thung/thung-nhua-da-nang-25l-2.jpg"
                                                   alt="Thùng Đa Năng 25L"
                                                   class="img-responsive">
                                             </div>
                                          </div>
                                       </div>
                                       <div class="item">
                                          <div class="image-wrapper">
                                             <div class="fixed-ratio">
                                                <img src="../uploads/7/household/thung/thung-nhua-da-nang-25l-1-90xauto.jpg"
                                                   data-full="../uploads/7/household/thung/thung-nhua-da-nang-25l-1.jpg"
                                                   alt="Thùng Đa Năng 25L"
                                                   class="img-responsive">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="full-image-container">
                                    <div class="image-wrapper">
                                       <img src="../uploads/7/household/thung/thung-nhua-da-nang-25l-2.jpg"
                                          alt="Thùng Đa Năng 25L"
                                          class="img-responsive">
                                    </div>
                                 </div>
                              </div>
                              <div class="basic-content">
                                 <div class="h3 main-category"><span></span></div>
                                 <div class="h1 product-name" data-product-title>Thùng Đa Năng 25L</div>
                                 <div class="info-group">
                                    <div class="product-sku">
                                       ID: <span data-product-sku style="text-transform: uppercase">b1066</span>
                                    </div>
                                    <div class="product-like">
                                       <a href="javascript:;"
                                          class="add-to-liked "
                                          data-url="../product/like/4008.html">
                                       <i class="fa fa-heart-o"></i>
                                       </a>
                                       <span
                                          class="total">2</span> lượt thích
                                    </div>
                                 </div>
                                 <div class="product-attributes"
                                    data-target="../product-variation/4008.json">
                                    <ul>
                                       <li class="visual-swatches-wrapper" data-type="visual">
                                          <div class="attribute-name">Màu sắc</div>
                                          <div class="attribute-values">
                                             <ul class="visual-swatch">
                                                <li data-slug="xám nhạt"
                                                   data-toggle="tooltip"
                                                   data-placement="top"
                                                   title="Xám  nhạt">
                                                   <div class="custom-radio">
                                                      <label>
                                                      <input type="radio" name="attribute_color" value="31">
                                                      <span style="background-color: rgb(171, 171, 171);"></span>
                                                      </label>
                                                   </div>
                                                </li>
                                                <li data-slug="xanh lam"
                                                   data-toggle="tooltip"
                                                   data-placement="top"
                                                   title="Xanh lam">
                                                   <div class="custom-radio">
                                                      <label>
                                                      <input type="radio" name="attribute_color" value="33">
                                                      <span style="background-color: rgb(21, 21, 169);"></span>
                                                      </label>
                                                   </div>
                                                </li>
                                             </ul>
                                          </div>
                                       </li>
                                    </ul>
                                 </div>
                                 <p>K&iacute;ch thước: 447&nbsp;x 337&nbsp;x 302&nbsp;mm</p>
                                 <p>Dung t&iacute;ch: 25000ml</p>
                                 <div class="buy-type">
                                    <ul>
                                       <li>
                                          <a href="#"
                                             type="button"
                                             data-toggle="modal"
                                             data-target="#distributors_popup"
                                             class="btn btn-outline">
                                          Mua số lượng nhỏ
                                          </a>
                                          <span>( >0 Cái
                                          )</span>
                                       </li>
                                       <li>
                                          <form
                                             action="https://www.daidongtien.com.vn/vi/add-to-cart/4008"
                                             method="POST">
                                             <input type="hidden" name="_token" value="1k5vUJeQY9f7j0H5ABQuhGodNh1IK9Z72G5pK2Eb">
                                             <input type="hidden" name="product_id" data-product-id>
                                             <input type="hidden" name="origin_product_id" value="4008">
                                             <button type="submit"
                                                class="btn btn-special">
                                             Mua số lượng lớn
                                             </button>
                                             <span>( >200 Cái
                                             )</span>
                                          </form>
                                       </li>
                                    </ul>
                                 </div>
                                 <div class="social-share socials">
                                    <div class="h3">Chia sẻ sản phẩm này</div>
                                    <ul>
                                       <li class="facebook">
                                          <a href="https://www.facebook.com/sharer/sharer.php?u=https://www.daidongtien.com.vn/vi/thung-da-nang-25l.html"
                                             target="_blank"
                                             title="Share via Facebook">
                                          <i class="fa fa-facebook"></i>
                                          </a>
                                       </li>
                                       <li class="twitter">
                                          <a href="https://twitter.com/home?status=https://www.daidongtien.com.vn/vi/thung-da-nang-25l.html"
                                             target="_blank"
                                             title="Share via Twitter">
                                          <i class="fa fa-twitter"></i>
                                          </a>
                                       </li>
                                       <li class="instagram">
                                          <a href="https://plus.google.com/share?url=https://www.daidongtien.com.vn/vi/thung-da-nang-25l.html"
                                             target="_blank"
                                             title="Share via Google Plus">
                                          <i class="fa fa-google-plus"></i>
                                          </a>
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="clearfix"></div>
                              <div class="product-content">
                                 <!-- Nav tabs -->
                                 <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active">
                                       <a href="#home" aria-controls="home" role="tab"
                                          data-toggle="tab">Mô tả</a>
                                    </li>
                                 </ul>
                                 <!-- Tab panes -->
                                 <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="home">
                                       <div class="row">
                                          <div class="col-lg-12">
                                             <p font-size:="" none="" open="" outline:="" style="box-sizing: border-box; margin: 0px 0px 10px; color: rgb(130, 130, 130); font-family: " text-align:=""><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:14px;"><span style="box-sizing: border-box; outline: none !important; color: rgb(0, 0, 0);"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important; font-weight: 700;">- TH&Agrave;NH PHẦN:</span></span></span></span></span></span></span></span></p>
                                             <p font-size:="" none="" open="" outline:="" style="box-sizing: border-box; margin: 0px 0px 10px; color: rgb(130, 130, 130); font-family: " text-align:=""><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:14px;"><span style="box-sizing: border-box; outline: none !important; color: rgb(0, 0, 0);"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important;">100% nguy&ecirc;n liệu nhựa PP cao cấp.&nbsp;Đặc biệt kh&ocirc;ng chứa chất BPA (nguy&ecirc;n nh&acirc;n g&acirc;y ra bệnh tiểu đường, ung thư v&agrave; v&ocirc; sinh nam), đảm bảo an to&agrave;n khi sử dụng</span></span></span></span></span></span></span></p>
                                             <p font-size:="" none="" open="" outline:="" style="box-sizing: border-box; margin: 0px 0px 10px; color: rgb(130, 130, 130); font-family: " text-align:=""><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:14px;"><span style="box-sizing: border-box; outline: none !important; color: rgb(0, 0, 0);"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important; font-weight: 700;">- C&Ocirc;NG DỤNG:&nbsp;</span></span></span></span></span></span></span></span></p>
                                             <p font-size:="" none="" open="" outline:="" style="box-sizing: border-box; margin: 0px 0px 10px; color: rgb(130, 130, 130); font-family: " text-align:=""><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:14px;"><span style="box-sizing: border-box; outline: none !important; color: rgb(0, 0, 0);"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important;">Được sử dụng chủ yếu&nbsp;để đựng nước.</span></span></span></span></span></span></span></p>
                                             <p font-size:="" none="" open="" outline:="" style="box-sizing: border-box; margin: 0px 0px 10px; color: rgb(130, 130, 130); font-family: " text-align:=""><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:14px;"><span style="box-sizing: border-box; outline: none !important; color: rgb(0, 0, 0);"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important; font-weight: 700;">- ĐIỂM NỔI BẬT:</span></span></span></span></span></span></span></span></p>
                                             <p font-size:="" none="" open="" outline:="" style="box-sizing: border-box; margin: 0px 0px 10px; color: rgb(130, 130, 130); font-family: " text-align:=""><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:14px;"><span style="box-sizing: border-box; outline: none !important; color: rgb(0, 0, 0);"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important;">Sản phẩm c&oacute; độ bền cao.</span></span></span></span></span></span></span></p>
                                             <p font-size:="" none="" open="" outline:="" style="box-sizing: border-box; margin: 0px 0px 10px; color: rgb(130, 130, 130); font-family: " text-align:=""><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:14px;"><span style="box-sizing: border-box; outline: none !important; color: rgb(0, 0, 0);"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important;">Quai x&aacute;ch của th&ugrave;ng chắc chắn&nbsp;</span><span style="box-sizing: border-box; outline: none !important;">gi&uacute;p dễ d&agrave;ng vận chuyển.</span></span></span></span></span></span></span></span></p>
                                             <p font-size:="" none="" open="" outline:="" style="box-sizing: border-box; margin: 0px 0px 10px; color: rgb(130, 130, 130); font-family: " text-align:=""><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:14px;"><span style="box-sizing: border-box; outline: none !important; color: rgb(0, 0, 0);"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important;">Khung nhựa vững chắc, chịu được va đập mạnh&nbsp;v&agrave; tải trọng tốt.&nbsp;</span></span></span></span></span></span></span></p>
                                             <p font-size:="" none="" open="" outline:="" style="box-sizing: border-box; margin: 0px 0px 10px; color: rgb(130, 130, 130); font-family: " text-align:=""><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:14px;"><span style="box-sizing: border-box; outline: none !important; color: rgb(0, 0, 0);"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important;">Th&ugrave;ng nhựa c&oacute; nắp d&agrave;y, chắc chắn bảo vệ đồ vật b&ecirc;n trong lu&ocirc;n sạch sẽ.</span></span></span></span></span></span></span></p>
                                             <p font-size:="" none="" open="" outline:="" style="box-sizing: border-box; margin: 0px 0px 10px; color: rgb(130, 130, 130); font-family: " text-align:=""><span style="font-family:arial,helvetica,sans-serif;"><span style="font-size:14px;"><span style="box-sizing: border-box; outline: none !important; color: rgb(0, 0, 0);"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important;"><span style="box-sizing: border-box; outline: none !important;">M&agrave;u sắc với 2 m&agrave;u ch&iacute;nh l&agrave;&nbsp;</span><span style="box-sizing: border-box; outline: none !important;">đỏ v&agrave; xanh dương tươi tắn,</span><span style="box-sizing: border-box; outline: none !important;">&nbsp;nổi bật tạo</span><span style="box-sizing: border-box; outline: none !important;">&nbsp;cho bạn cảm gi&aacute;c&nbsp;</span><span style="box-sizing: border-box; outline: none !important;">thoải m&aacute;i&nbsp;</span><span style="box-sizing: border-box; outline: none !important;">đầy sức sống.</span></span></span></span></span></span></span></span></p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="related-products">
                                 <div class="h3">Sản phẩm tương tự</div>
                                 <ul>
                                    <li>
                                       <div class="wrapper">
                                          <div class="product-image">
                                             <div class="image-wrapper">
                                                <a href="thung-vuong-160l.html"
                                                   title="Thùng vuông 160L">
                                                <img
                                                   src="../uploads/7/ca-nhua/thung-ket-song/thung-vuong-160l-1-150xauto.jpg"
                                                   alt="Thùng vuông 160L"
                                                   width="90"
                                                   class="img-responsive">
                                                </a>
                                             </div>
                                          </div>
                                          <div class="product-info">
                                             <div class="product-name">
                                                <a href="thung-vuong-160l.html"
                                                   title="Thùng vuông 160L">
                                                <span>Thùng vuông 160L</span>
                                                </a>
                                             </div>
                                             <div class="product-like">
                                                <a href="javascript:;"
                                                   class="add-to-liked "
                                                   data-url="../product/like/735.html">
                                                <i class="fa fa-heart-o"></i>
                                                </a>
                                                <span
                                                   class="total">1</span> lượt thích
                                             </div>
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="wrapper">
                                          <div class="product-image">
                                             <div class="image-wrapper">
                                                <a href="thung-vuong-50l.html"
                                                   title="Thùng Vuông 50L">
                                                <img
                                                   src="../uploads/7/ca-nhua/thung-ket-song/thung-vuong-50l-4-150xauto.jpg"
                                                   alt="Thùng Vuông 50L"
                                                   width="90"
                                                   class="img-responsive">
                                                </a>
                                             </div>
                                          </div>
                                          <div class="product-info">
                                             <div class="product-name">
                                                <a href="thung-vuong-50l.html"
                                                   title="Thùng Vuông 50L">
                                                <span>Thùng Vuông 50L</span>
                                                </a>
                                             </div>
                                             <div class="product-like">
                                                <a href="javascript:;"
                                                   class="add-to-liked "
                                                   data-url="../product/like/736.html">
                                                <i class="fa fa-heart-o"></i>
                                                </a>
                                                <span
                                                   class="total">0</span> lượt thích
                                             </div>
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="wrapper">
                                          <div class="product-image">
                                             <div class="image-wrapper">
                                                <a href="thung-60l.html"
                                                   title="Thùng 60L">
                                                <img
                                                   src="../uploads/2/household/thung/thung-60l-1-150xauto.jpg"
                                                   alt="Thùng 60L"
                                                   width="90"
                                                   class="img-responsive">
                                                </a>
                                             </div>
                                          </div>
                                          <div class="product-info">
                                             <div class="product-name">
                                                <a href="thung-60l.html"
                                                   title="Thùng 60L">
                                                <span>Thùng 60L</span>
                                                </a>
                                             </div>
                                             <div class="product-like">
                                                <a href="javascript:;"
                                                   class="add-to-liked "
                                                   data-url="../product/like/836.html">
                                                <i class="fa fa-heart-o"></i>
                                                </a>
                                                <span
                                                   class="total">3</span> lượt thích
                                             </div>
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="wrapper">
                                          <div class="product-image">
                                             <div class="image-wrapper">
                                                <a href="thung-35l.html"
                                                   title="Thùng 35L">
                                                <img
                                                   src="../uploads/2/household/thung/thung-35l-1-150xauto.jpg"
                                                   alt="Thùng 35L"
                                                   width="90"
                                                   class="img-responsive">
                                                </a>
                                             </div>
                                          </div>
                                          <div class="product-info">
                                             <div class="product-name">
                                                <a href="thung-35l.html"
                                                   title="Thùng 35L">
                                                <span>Thùng 35L</span>
                                                </a>
                                             </div>
                                             <div class="product-like">
                                                <a href="javascript:;"
                                                   class="add-to-liked "
                                                   data-url="../product/like/851.html">
                                                <i class="fa fa-heart-o"></i>
                                                </a>
                                                <span
                                                   class="total">1</span> lượt thích
                                             </div>
                                          </div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="wrapper">
                                          <div class="product-image">
                                             <div class="image-wrapper">
                                                <a href="thung-25l.html"
                                                   title="Thùng 25L">
                                                <img
                                                   src="../uploads/2/household/thung/thung-25l-1-150xauto.jpg"
                                                   alt="Thùng 25L"
                                                   width="90"
                                                   class="img-responsive">
                                                </a>
                                             </div>
                                          </div>
                                          <div class="product-info">
                                             <div class="product-name">
                                                <a href="thung-25l.html"
                                                   title="Thùng 25L">
                                                <span>Thùng 25L</span>
                                                </a>
                                             </div>
                                             <div class="product-like">
                                                <a href="javascript:;"
                                                   class="add-to-liked "
                                                   data-url="../product/like/869.html">
                                                <i class="fa fa-heart-o"></i>
                                                </a>
                                                <span
                                                   class="total">4</span> lượt thích
                                             </div>
                                          </div>
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                  </section>
                  <div class="modal fade" tabindex="-1" role="dialog" id="distributors_popup">
                     <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                           <span aria-hidden="true">&times;</span>
                           </button>
                           <div class="modal-body">
                              <div class="wrapper">
                                 <div class="map-wrapper">
                                    <div class="the-map" id="store_locator_map"></div>
                                 </div>
                                 <div class="details-wrapper">
                                    <div class="product-name">
                                       <p class="h1">Thùng Đa Năng 25L</p>
                                       <p class="h5">ID: b1066</p>
                                    </div>
                                    <form class="address-selector" action="https://www.daidongtien.com.vn/store-locators/render" method="GET">
                                       <div class="form-group">
                                          <div class="col-xs-5">
                                             <label for="province_selector">Tỉnh</label>
                                          </div>
                                          <div class="col-xs-7">
                                             <div class="custom-select">
                                                <select id="province_selector" class="form-control" name="province_id">
                                                   <option value="" selected="selected"></option>
                                                   <option value="34">An Giang</option>
                                                   <option value="24">Bà Rịa-Vũng Tàu</option>
                                                   <option value="14">Bạc Liêu</option>
                                                   <option value="18">Bến Tre</option>
                                                   <option value="42">Bình Dương</option>
                                                   <option value="28">Bình Định</option>
                                                   <option value="19">Bình Phước</option>
                                                   <option value="10">Bình Thuận</option>
                                                   <option value="29">Cà Mau</option>
                                                   <option value="7">Cần Thơ</option>
                                                   <option value="25">Đà Nẵng</option>
                                                   <option value="16">Đắk Lắk</option>
                                                   <option value="12">Đắk Nông</option>
                                                   <option value="5">Đồng Nai</option>
                                                   <option value="26">Đồng Tháp</option>
                                                   <option value="46">Gia Lai</option>
                                                   <option value="45">Hà Nam</option>
                                                   <option value="22">Hà Nội</option>
                                                   <option value="13">Hà Tĩnh</option>
                                                   <option value="15">Hải Dương</option>
                                                   <option value="6">Hậu Giang</option>
                                                   <option value="31">Hồ Chí Minh</option>
                                                   <option value="11">Khánh Hòa</option>
                                                   <option value="4">Kiên Giang</option>
                                                   <option value="37">Kon Tum</option>
                                                   <option value="27">Lâm Đồng</option>
                                                   <option value="17">Long An</option>
                                                   <option value="36">Nam Định</option>
                                                   <option value="41">Nghệ An</option>
                                                   <option value="40">Ninh Bình</option>
                                                   <option value="9">Ninh Thuận</option>
                                                   <option value="44">Phú Yên</option>
                                                   <option value="20">Quảng Bình</option>
                                                   <option value="30">Quảng Nam</option>
                                                   <option value="32">Quảng Ngãi</option>
                                                   <option value="33">Quảng Trị</option>
                                                   <option value="38">Sóc Trăng</option>
                                                   <option value="3">Tây Ninh</option>
                                                   <option value="43">Thái Bình</option>
                                                   <option value="35">Thanh Hóa</option>
                                                   <option value="39">Thừa Thiên Huế</option>
                                                   <option value="8">Tiền Giang</option>
                                                   <option value="21">Trà Vinh</option>
                                                   <option value="23">Vinh Long</option>
                                                </select>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="form-group">
                                          <div class="col-xs-5">
                                             <label for="ward_selector">Quận / Huyện</label>
                                          </div>
                                          <div class="col-xs-7">
                                             <div class="custom-select">
                                                <select id="ward_selector" class="form-control" name="district_id">
                                                   <option value="" selected="selected"></option>
                                                </select>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="form-group">
                                          <div class="col-xs-5">
                                             <label for="ward_selector">Phường / Xã</label>
                                          </div>
                                          <div class="col-xs-7">
                                             <div class="custom-select">
                                                <select id="ward_selector" class="form-control" name="ward_id">
                                                   <option value="" selected="selected"></option>
                                                </select>
                                             </div>
                                          </div>
                                       </div>
                                    </form>
                                    <div class="distributors-recommend">
                                       <div class="title">
                                          <b>Cửa hàng gần nhất:</b>
                                          <span>0</span>
                                       </div>
                                       <div class="distributors-list js-custom-scrollbar">
                                          <ul class="items">
                                          </ul>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </main>
@endsection