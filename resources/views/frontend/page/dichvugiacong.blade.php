@extends('frontend.master')
@section('content')
 <main class="main" id="main">
                  <section class="page-section page-title">
                     <div class="container">
                        <h1>&nbsp;</h1>
                        <ul class="breadcrumb">
                           <li><a title="Trang chủ" href="../vi.html">Trang chủ</a></li>
                           <li><span>Dịch vụ</span></li>
                           <li><span>Dịch vụ Gia công</span></li>
                        </ul>
                     </div>
                  </section>
                  <section class="page-section main-section">
                     <div class="main-content">
                        <div class="container">
                           <div class="title-with-description">
                              <h1 class="h1">Dịch vụ OEM/ODM</h1>
                              <div class="description">
                                 <p style="text-align: center;">
                                    <style type="text/css">p.p1 {margin: 0.0px 0.0px 0.0px 0.0px; text-align: center; line-height: 25.0px; font: 14.0px 'SVN-Helvetica Neue'; color: #949494}</style>
                                 </p>
                                 <p class="p1"><span style="font-size:18px;">Với hơn 35 năm kinh nghiệm trong việc gia c&ocirc;ng sản phẩm Nhựa. Đại Đồng Tiến cung cấp cho kh&aacute;ch h&agrave;ng một dịch vụ gia c&ocirc;ng từ kh&acirc;u l&ecirc;n &yacute; tưởng, thiết kế cho đến khi ra sản phẩm cuối c&ugrave;ng theo y&ecirc;u cầu chuy&ecirc;n biệt của từng kh&aacute;ch h&agrave;ng. Ch&uacute;ng t&ocirc;i tự tin mang đến cho kh&aacute;ch h&agrave;ng những sản phẩm kh&ocirc;ng chỉ đẹp về kiểu d&aacute;ng, h&igrave;nh thức m&agrave; c&ograve;n đảm bảo chất lượng cao cấp nhất.</span></p>
                              </div>
                           </div>
                           <img src="uploads/2/page-templates/dich-vu-phan-phoi/qui-trinh-01.png"
                              alt="Dịch vụ Gia công"
                              class="img-responsive">
                        </div>
                     </div>
                     <div class="content-with-background contact-form-container"
                        style="background-image: url('uploads/1/page-templates/service-distribution/service-dis-consult.png')">
                        <div class="title-with-description">
                           <h3 class="h1">BẠN CẦN TƯ VẤN?</h3>
                        </div>
                        <form action="https://www.daidongtien.com.vn/contact-forms/send"
                           method="POST"
                           class="contact-form"
                           enctype="multipart/form-data">
                           <input type="hidden" name="_token" value="1k5vUJeQY9f7j0H5ABQuhGodNh1IK9Z72G5pK2Eb">
                           <div class="wrapper">
                              <div class="form-group">
                                 <input type="text" class="form-control" name="name" placeholder="Tên *" required>
                              </div>
                              <div class="form-group">
                                 <input type="text" class="form-control" name="email" placeholder="Email *" required>
                              </div>
                              <div class="form-group">
                                 <input type="text" class="form-control" name="phone" placeholder="Số Điện Thoại">
                              </div>
                              <div class="form-group">
                                 <textarea name="content"
                                    rows="6"
                                    class="form-control"
                                    minlength="10"
                                    placeholder="Nội dung *" required></textarea>
                              </div>
                              <div class="form-group select-file-group">
                                 <div class="select-file-label">Đính kèm File:</div>
                                 <div class="custom-select-file">
                                    <label>
                                    <input type="file"
                                       class="form-control"
                                       name="attachments[]"
                                       multiple
                                       data-multiple-caption="{count} files selected">
                                    <span class="file-info">0 Tập tin được chọn</span>
                                    <span class="btn-file">Chọn</span>
                                    </label>
                                 </div>
                                 <div class="select-file-label">Tập tin hình ảnh, pdf, word, excel,... tải lên tối đa 2MB</div>
                              </div>
                              <div class="form-actions">
                                 <button type="submit" class="btn-special">Gửi tin nhắn</button>
                                 <a href="#" class="clear">
                                 Xóa <i class="glyphicon glyphicon-remove"></i>
                                 </a>
                              </div>
                           </div>
                        </form>
                     </div>
                     <div class="items-list-slider partners-list">
                        <div class="container">
                           <div class="title-with-description">
                              <h3 class="h1">Đối tác</h3>
                           </div>
                           <div class="js-slider-brands-list">
                              <div class="item">
                                 <img src="../uploads/2/page-templates/doi-tac/web-01-3.png"
                                    alt="Unilever"
                                    class="">
                                 <img src="../uploads/2/page-templates/doi-tac/web-09-1.png"
                                    alt="TOA"
                                    class="">
                              </div>
                              <div class="item">
                                 <img src="../uploads/2/page-templates/doi-tac/web-04-1.png"
                                    alt="NESTLE"
                                    class="">
                                 <img src="../uploads/2/page-templates/doi-tac/web-05-1.png"
                                    alt="Sofia"
                                    class="">
                              </div>
                              <div class="item">
                                 <img src="../uploads/2/page-templates/doi-tac/web-08-1.png"
                                    alt="Vietnamobile"
                                    class="">
                                 <img src="../uploads/2/page-templates/doi-tac/web-07-1.png"
                                    alt="Woolworths"
                                    class="">
                              </div>
                              <div class="item">
                                 <img src="../uploads/2/page-templates/doi-tac/web-06-1.png"
                                    alt=""
                                    class="">
                                 <img src="../uploads/2/page-templates/doi-tac/web-03-2.png"
                                    alt=""
                                    class="">
                              </div>
                              <div class="item">
                                 <img src="../uploads/2/page-templates/doi-tac/web-09-1.png"
                                    alt=""
                                    class="">
                                 <img src="../uploads/2/page-templates/doi-tac/web-04-1.png"
                                    alt=""
                                    class="">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="items-list-slider certificates-list">
                        <div class="container">
                           <div class="title-with-description">
                              <h3 class="h1">Chứng nhận</h3>
                           </div>
                           <div class="js-slider-brands-list">
                              <div class="item">
                                 <img src="../uploads/1/page-templates/service-distribution/service-dis-certifcates-iso9001-1-1.png"
                                    title="ISO 9001 : 2008"
                                    alt="ISO 9001 : 2008">
                                 <img src="../uploads/1/page-templates/service-distribution/service-dis-certificates-14001.png"
                                    title="ISO 14001 : 2015"
                                    alt="ISO 14001 : 2015">
                              </div>
                              <div class="item">
                                 <img src="../uploads/1/page-templates/service-distribution/service-dis-certificates-bsci.png"
                                    title="BSCI REPORT"
                                    alt="BSCI REPORT">
                                 <img src="../uploads/1/page-templates/service-distribution/service-dis-certificates-fda-2.png"
                                    title="FDA CERTIFICATION"
                                    alt="FDA CERTIFICATION">
                              </div>
                              <div class="item">
                                 <img src="../uploads/1/page-templates/service-distribution/service-dis-certificates-icti-3.png"
                                    title="ICTI CARE CERTIFICATION"
                                    alt="ICTI CARE CERTIFICATION">
                                 <img src="../uploads/1/page-templates/service-distribution/service-dis-certificates-meta-1.png"
                                    title="SMETA 4 PILLARS STANDARD"
                                    alt="SMETA 4 PILLARS STANDARD">
                              </div>
                              <div class="item">
                                 <img src="../uploads/1/page-templates/service-distribution/service-dis-certificates-lfbg-2.png"
                                    title="LFGB CERTIFICATION"
                                    alt="LFGB CERTIFICATION">
                                 <img src="../uploads/1/page-templates/service-distribution/service-dis-certificates.png"
                                    title="VIET NAM’S TOP 500 LARGEST ENTERPRISES"
                                    alt="VIET NAM’S TOP 500 LARGEST ENTERPRISES">
                              </div>
                              <div class="item">
                                 <img src="../uploads/1/page-templates/service-distribution/service-dis-certificates-vietnamclc.png"
                                    title="Hàng Việt Nam Chất lượng cao"
                                    alt="Hàng Việt Nam Chất lượng cao">
                                 <img src="../uploads/1/page-templates/service-distribution/service-dis-certificates-cr.png"
                                    title="Chứng nhận hợp qui CR"
                                    alt="Chứng nhận hợp qui CR">
                              </div>
                           </div>
                        </div>
                     </div>
                  </section>
               </main>
@endsection