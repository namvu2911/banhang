@extends('frontend.master')
@section('content')
<main class="main" id="main">
                  <div class="homepage-slider">
                     <div class="slider-wrapper js-homepage-slider">
                     @foreach($banner as $item)
                        <div class="item">
                           <div class="main-image">
                              <img src="uploads/2/page-templates/home-1/{{$item->image}}"
                                 class="hidden-xs"
                                 alt="Trang chủ">
                              <img src="{{$item->url_mobile}}"
                                 class="visible-xs"
                                 alt="Trang chủ">
                           </div>
                        </div>
                     @endforeach
                     </div>
                     <div class="slider-brands-list">
                        <ul>
                           <li>
                              <a href="#"
                                 title="FDA" target="_blank">
                              <img src="uploads/1/25395497-293532501167136-2089193649-n.png"
                                 alt="FDA"
                                 class="img-responsive">
                              </a>
                           </li>
                           <li>
                              <a href="http://www.icti-care.org/e/default.asp"
                                 title="CR" target="_blank">
                              <img src="uploads/1/25394410-293529627834090-1423059547-n.png"
                                 alt="CR"
                                 class="img-responsive">
                              </a>
                           </li>
                           <li>
                              <a href="http://www.vnr500.com.vn/"
                                 title="Icti care" target="_blank">
                              <img src="uploads/1/25395232-293526787834374-802964127-n.png"
                                 alt="Icti care"
                                 class="img-responsive">
                              </a>
                           </li>
                        </ul>
                     </div>
                     <div class="description">
                        <div class="title">Thiết kế - Phân phối - Công nghệ</div>
                        <div class="text-content">
                           Đại Đồng Tiến tự hào là doanh nghiệp Nhựa đi đầu trong Thiết kế - Phân phối - Công nghệ
                        </div>
                     </div>
                  </div>
                  <div class="homepage-section about-us-section full-height-section">
                     <img src="uploads/1/page-templates/home-1/home-aboutus.png"
                        alt="Về Chúng tôi"
                        class="homepage-section-bg">
                     <div class="iframe-container"
                        data-src="https://www.youtube.com/embed/5phQgpoASuM?rel=0&amp;autoplay=1"></div>
                     <div class="section-content about-us-content">
                        <div class="title-with-description">
                           <h3 class="h1">Về Chúng tôi</h3>
                           <div class="description">
                              Kiến tạo chất lượng từ 1983 -  Với tầm nhìn chiến lược dài hạn, phương châm quản lí linh hoạt và nắm bắt được những yêu cầu đổi mới trong giai đoạn toàn cầu hóa nền kinh tế - Đại Đồng Tiến tự hào là một trong các doanh nghiệp tiên phong đóng góp đáng kể cho những thành công vượt bậc về mặt kinh tế của đất nước để trở thành một trong những thương hiệu dẫn đầu trong ngành sản xuất nhựa Việt Nam ngày nay.
                           </div>
                        </div>
                        <div class="play-video">
                           <a href="#"
                              data-target=".about-us-section .iframe-container"
                              class="js-trigger-play-video"
                              title="Play Video">
                           <i class="fa fa-play-circle-o"></i>
                           <span>
                           Play Video
                           </span>
                           </a>
                        </div>
                     </div>
                  </div>
                  <div class="homepage-section full-height-section categories-section">
                     <img src="uploads/2/page-templates/home-1/hinh-web-01final.png"
                        alt=""
                        class="homepage-section-bg">
                     <div class="section-content">
                        <div class="wrapper">
                           <div class="title-with-description">
                              <h3 class="h1">
                                 Sản phẩm
                              </h3>
                           </div>
                           <ul>
                              @foreach($cat_pro as $pro)
                              <li>
                                 <a href="{{$pro->id}}"
                                    title="{{$pro->slug}}">
                                 <span class="icon">
                                 <img src="uploads/1/page-templates/home-1/{{$pro->icon}}"
                                    alt="{{$pro->name}}"
                                    class="img-responsive">
                                 <img src="public/uploads/1/page-templates/home-1/{{$pro->icon}}-hover.png"
                                    alt="{{$pro->name}}"
                                    class="img-responsive hover">
                                 </span>
                                 <span class="text">{{$pro->name}}</span>
                                 </a>
                              </li>
                             @endforeach
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="service-section">
                     <div class="container">
                        <div class="wrapper">
                           <div class="title-with-description">
                              <h3 class="h1">Dịch vụ</h3>
                           </div>
                           <ul>
                              <li>
                                 <div class="number">
                                    <img src="uploads/2/page-templates/home-1/web-02-03.png"
                                       alt="Phân phối"
                                       class="img-responsive">
                                 </div>
                                 <div class="content">
                                    <div class="title">Phân phối</div>
                                    <div class="description js-truncate-text">
                                       Bạn có hàng hóa và muốn tìm các kênh phân phối, quảng cáo sản phẩm tại thị trường Việt Nam? Chúng tôi luôn sẵn sàng hợp tác bằng các dịch vụ phân phối bằng mạng lưới đối tác trong nước.
                                    </div>
                                    <a href="{{route('dich-vu-phan-phoi')}}"
                                       title="Phân phối"
                                       class="btn-special">
                                    Xem thêm
                                    </a>
                                 </div>
                              </li>
                              <li>
                                 <div class="number">
                                    <img src="public/uploads/2/page-templates/home-1/web-01-03.png"
                                       alt="Gia công "
                                       class="img-responsive">
                                 </div>
                                 <div class="content">
                                    <div class="title">Gia công </div>
                                    <div class="description js-truncate-text">
                                       Bạn cần một đối tác chiến lược đáng tin cậy và kinh nghiệm về thiết kế, sản xuất và kiểm soát chất lượng? Ở Đại Đồng Tiến, bạn có thể tìm thấy tất cả những điều này.
                                    </div>
                                    <a href="{{route('dich-vu-gia-cong')}}"
                                       title="Gia công "
                                       class="btn-special">
                                    Xem thêm
                                    </a>
                                 </div>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div id="waypoint" class="homepage-section full-height-section why-choose-us-section">
                     <div class="container">
                        <div class="wrapper">
                           <div class="title-with-description">
                              <h3 class="h1">Tại sao chọn chúng tôi?</h3>
                           </div>
                           <div class="image">
                              <div class="counting">
                                 <ul class="list-unstyled">
                                    <li>
                                       <div class="counting-wrapper">
                                          <div class="icon-number-group">
                                             <div class="icon">
                                                <img src="public/uploads/1/brands/imgearthat3x.png"
                                                   width="55px"
                                                   height="55px"
                                                   alt="Quốc gia xuất khẩu">
                                             </div>
                                             <div class="number js-text-fill" data-max-font-pixels="65">
                                                <span data-number="60">60</span>
                                             </div>
                                          </div>
                                          <div class="text">Quốc gia xuất khẩu</div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="counting-wrapper">
                                          <div class="icon-number-group">
                                             <div class="icon">
                                                <img src="public/uploads/1/brands/imgyearat3x.png"
                                                   width="55px"
                                                   height="55px"
                                                   alt="Năm kinh nghiệm">
                                             </div>
                                             <div class="number js-text-fill" data-max-font-pixels="65">
                                                <span data-number="35">35</span>
                                             </div>
                                          </div>
                                          <div class="text">Năm kinh nghiệm</div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="counting-wrapper">
                                          <div class="icon-number-group">
                                             <div class="icon">
                                                <img src="public/uploads/1/brands/imgpersonat3x.png"
                                                   width="55px"
                                                   height="55px"
                                                   alt="Phần trăm Khách hàng đánh giá sản phẩm ‘chất lượng’">
                                             </div>
                                             <div class="number js-text-fill" data-max-font-pixels="65">
                                                <span data-number="90">90</span>
                                             </div>
                                          </div>
                                          <div class="text">Phần trăm Khách hàng đánh giá sản phẩm ‘chất lượng’</div>
                                       </div>
                                    </li>
                                    <li>
                                       <div class="counting-wrapper">
                                          <div class="icon-number-group">
                                             <div class="icon">
                                                <img src="public/uploads/1/brands/imgproductat3x.png"
                                                   width="55px"
                                                   height="55px"
                                                   alt="Triệu sản phẩm được tiêu thụ tại Việt Nam năm 2016">
                                             </div>
                                             <div class="number js-text-fill" data-max-font-pixels="65">
                                                <span data-number="200">200</span>
                                             </div>
                                          </div>
                                          <div class="text">Triệu sản phẩm được tiêu thụ tại Việt Nam năm 2016</div>
                                       </div>
                                    </li>
                                 </ul>
                              </div>
                              <img src="public/uploads/1/heart-dai-dong-tien.png"
                                 alt="Tại sao chọn chúng tôi?"
                                 class="img-responsive img-bg">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="items-list-slider partners-list">
                     <div class="container">
                        <div class="title-with-description">
                           <h3 class="h1">Đối tác</h3>
                        </div>
                        <div class="js-slider-brands-list">
                           <div class="item">
                              <img src="public/uploads/2/page-templates/doi-tac/web-01-3.png"
                                 alt="Unilever"
                                 class="img-white">
                              <img src="public/uploads/2/page-templates/doi-tac/web-09-1.png"
                                 alt="TOA"
                                 class="img-white">
                           </div>
                           <div class="item">
                              <img src="public/uploads/2/page-templates/doi-tac/web-04-1.png"
                                 alt="NESTLE"
                                 class="img-white">
                              <img src="public/uploads/2/page-templates/doi-tac/web-05-1.png"
                                 alt="Sofia"
                                 class="img-white">
                           </div>
                           <div class="item">
                              <img src="public/uploads/2/page-templates/doi-tac/web-08-1.png"
                                 alt="Vietnamobile"
                                 class="img-white">
                              <img src="public/uploads/2/page-templates/doi-tac/web-07-1.png"
                                 alt="Woolworths"
                                 class="img-white">
                           </div>
                           <div class="item">
                              <img src="public/uploads/2/page-templates/doi-tac/web-06-1.png"
                                 alt=""
                                 class="img-white">
                              <img src="public/uploads/2/page-templates/doi-tac/web-03-2.png"
                                 alt=""
                                 class="img-white">
                           </div>
                           <div class="item">
                              <img src="public/uploads/2/page-templates/doi-tac/web-09-1.png"
                                 alt=""
                                 class="img-white">
                              <img src="public/uploads/2/page-templates/doi-tac/web-04-1.png"
                                 alt=""
                                 class="img-white">
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="items-list-slider certificates-list">
                     <div class="container">
                        <div class="title-with-description">
                           <h3 class="h1">Chứng nhận</h3>
                        </div>
                        <div class="js-slider-brands-list">
                           <div class="item">
                              <img src="public/uploads/1/page-templates/service-distribution/service-dis-certifcates-iso9001-1-1.png"
                                 title="ISO 9001 : 2008"
                                 alt="ISO 9001 : 2008">
                              <img src="public/uploads/1/page-templates/service-distribution/service-dis-certificates-14001.png"
                                 title="ISO 14001 : 2015"
                                 alt="ISO 14001 : 2015">
                           </div>
                           <div class="item">
                              <img src="public/uploads/1/page-templates/service-distribution/service-dis-certificates-bsci.png"
                                 title="BSCI REPORT"
                                 alt="BSCI REPORT">
                              <img src="public/uploads/1/page-templates/service-distribution/service-dis-certificates-fda-2.png"
                                 title="FDA CERTIFICATION"
                                 alt="FDA CERTIFICATION">
                           </div>
                           <div class="item">
                              <img src="public/uploads/1/page-templates/service-distribution/service-dis-certificates-icti-3.png"
                                 title="ICTI CARE CERTIFICATION"
                                 alt="ICTI CARE CERTIFICATION">
                              <img src="public/uploads/1/page-templates/service-distribution/service-dis-certificates-meta-1.png"
                                 title="SMETA 4 PILLARS STANDARD"
                                 alt="SMETA 4 PILLARS STANDARD">
                           </div>
                           <div class="item">
                              <img src="public/uploads/1/page-templates/service-distribution/service-dis-certificates-lfbg-2.png"
                                 title="LFGB CERTIFICATION"
                                 alt="LFGB CERTIFICATION">
                              <img src="public/uploads/1/page-templates/service-distribution/service-dis-certificates.png"
                                 title="VIET NAM’S TOP 500 LARGEST ENTERPRISES"
                                 alt="VIET NAM’S TOP 500 LARGEST ENTERPRISES">
                           </div>
                           <div class="item">
                              <img src="public/uploads/1/page-templates/service-distribution/service-dis-certificates-vietnamclc.png"
                                 title="Hàng Việt Nam Chất lượng cao"
                                 alt="Hàng Việt Nam Chất lượng cao">
                              <img src="public/uploads/1/page-templates/service-distribution/service-dis-certificates-cr.png"
                                 title="Chứng nhận hợp qui CR"
                                 alt="Chứng nhận hợp qui CR">
                           </div>
                        </div>
                     </div>
                  </div>
               </main>
@endsection