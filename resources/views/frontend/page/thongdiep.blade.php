@extends('frontend.master')
@section('content')
   <main class="main" id="main">
                  <section class="page-section page-title">
                     <div class="container">
                        <h1>Thông điệp từ CEO</h1>
                        <ul class="breadcrumb">
                           <li><a title="Trang chủ" href="../vi.html">Trang chủ</a></li>
                           <li><span>Thông điệp từ CEO</span></li>
                        </ul>
                     </div>
                  </section>
                  <section class="page-section main-section">
                     <div class="container">
                        <div class="main-content">
                           <div class="about-us-message">
                              <div class="avatar">
                                 <img src="public/uploads/1/page-templates/about-us/message-from-ceo/messageceo-avatar-1-1.png"
                                    alt="Mr.Christopher Trinh"
                                    class="img-responsive">
                              </div>
                              <div class="content">
                                 <div class="description">
                                    <p style="text-align: justify;"><span style="font-size:16px;"><span style="font-family:arial,helvetica,sans-serif;">T&ecirc;n Đại Đồng Tiến thể hiện t&acirc;m huyết của (c&aacute;c) nh&agrave; s&aacute;ng lập. Đ&oacute; l&agrave; sự đam m&ecirc; v&agrave; theo đuổi mục ti&ecirc;u: kiến tạo những sản phẩm, những gi&aacute; trị kh&aacute;c biệt v&agrave; vượt trội d&agrave;nh cho mọi người.Hơn 30 năm ph&aacute;t triển, từ một tổ hợp sản xuất nhỏ, Đại Đồng Tiến đ&atilde; bứt ph&aacute; để trở th&agrave;nh c&ocirc;ng ty c&oacute; thương hiệu dẫn đầu trong ng&agrave;nh c&ocirc;ng nghiệp sản xuất nhựa tại Việt Nam.Tiếp tục ki&ecirc;n định với t&acirc;m huyết từ những ng&agrave;y khởi nghiệp, Đại Đồng Tiến lu&ocirc;n hướng tới c&aacute;c mục ti&ecirc;u:</span></span></p>
                                    <ul>
                                       <li style="text-align: justify;"><span style="font-size:16px;"><span style="font-family:arial,helvetica,sans-serif;">Gi&aacute; trị cho cuộc sống</span></span></li>
                                       <li style="text-align: justify;"><span style="font-size:16px;"><span style="font-family:arial,helvetica,sans-serif;">S&aacute;ng tạo từng ng&agrave;y</span></span></li>
                                       <li style="text-align: justify;"><span style="font-size:16px;"><span style="font-family:arial,helvetica,sans-serif;">Tr&aacute;ch nhiệm với x&atilde; hội</span></span></li>
                                    </ul>
                                 </div>
                                 <div class="author">Mr.Christopher Trinh</div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </section>
               </main>
@endsection