@extends('frontend.master')
@section('content')
<main class="main" id="main">
                  <section class="page-section page-title with-image">
                     <img src="public/uploads/1/page-templates/contact/contact-banner.png" alt="" class="img-responsive">
                     <div class="container">
                     </div>
                  </section>
                  <section class="page-section main-section">
                     <div class="container">
                        <div class="main-content">
                           <div class="addresses-list">
                              <ul>
                                 <li>
                                    <div class="title">
                                       <i class="fa fa-map-marker"></i>
                                       Địa chỉ
                                    </div>
                                    <p>216 Tân Thành, Phường 15, Quận 5, Tp. HCM</p>
                                 </li>
                                 <li>
                                    <div class="title">
                                       <i class="fa fa-phone"></i>
                                       Điện thoại
                                    </div>
                                    <div class="h3">Tel: 0906 600 183</div>
                                    <div class="h3">Hotline: 1800 588 893</div>
                                 </li>
                                 <li>
                                    <div class="title">
                                       <i class="glyphicon glyphicon-envelope"></i>
                                       Email
                                    </div>
                                    <p><a href="mailto:customerservice@daidongtien.com">customerservice@daidongtien.com</a></p>
                                 </li>
                              </ul>
                           </div>
                           <div class="contact-data">
                              <div class="map">
                                 <div class="wrapper">
                                    <h2 class="h2">TÌM ĐƯỜNG ĐI</h2>
                                    <div class="map-wrapper">
                                       <a href="#" data-toggle="modal" data-target="#contact_map_modal">
                                       <img src="public/uploads/7/ban-do/map-cty-2.png"
                                          alt="">
                                       </a>
                                    </div>
                                 </div>
                              </div>
                              <form action="https://www.daidongtien.com.vn/contact-forms/send"
                                 method="POST"
                                 class="contact-form"
                                 enctype="multipart/form-data">
                                 <input type="hidden" name="_token" value="1k5vUJeQY9f7j0H5ABQuhGodNh1IK9Z72G5pK2Eb">
                                 <div class="wrapper">
                                    <h2 class="h2">CHÚNG TÔI CÓ THỂ GIÚP GÌ CHO BẠN?</h2>
                                    <div class="form-group">
                                       <input type="text" class="form-control" name="name" placeholder="Tên *" required>
                                    </div>
                                    <div class="form-group">
                                       <input type="text" class="form-control" name="email" placeholder="Email *" required>
                                    </div>
                                    <div class="form-group">
                                       <input type="text" class="form-control" name="phone" placeholder="Số Điện Thoại">
                                    </div>
                                    <div class="form-group">
                                       <textarea name="content"
                                          rows="6"
                                          class="form-control"
                                          minlength="10"
                                          placeholder="Nội dung *" required></textarea>
                                    </div>
                                    <div class="form-group select-file-group">
                                       <div class="select-file-label">Đính kèm File:</div>
                                       <div class="custom-select-file">
                                          <label>
                                          <input type="file"
                                             class="form-control"
                                             name="attachments[]"
                                             multiple
                                             data-multiple-caption="{count} files selected">
                                          <span class="file-info">0 Tập tin được chọn</span>
                                          <span class="btn-file">Chọn</span>
                                          </label>
                                       </div>
                                    </div>
                                    <div class="select-file-label" style="color:#828282;text-align:right">Tập tin hình ảnh, pdf, word, excel,... tải lên tối đa 2MB</div>
                                    <div class="form-actions">
                                       <button type="submit" class="btn-special">Gửi tin nhắn</button>
                                       <a href="#" class="clear">
                                       Xóa <i class="glyphicon glyphicon-remove"></i>
                                       </a>
                                    </div>
                                 </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </section>
               </main>
@endsection