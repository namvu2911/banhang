@extends('frontend.master')
@section('content')
 <main class="main" id="main">
                  <section class="page-section page-title">
                     <div class="container">
                        <h1>Tin tức</h1>
                        <ul class="breadcrumb">
                           <li><a title="Trang chủ" href="../vi.html">Trang chủ</a></li>
                           <li><span>Tin tức</span></li>
                        </ul>
                     </div>
                  </section>
                  <section class="page-section main-section columns2">
                     <div class="container">
                        <div class="main-content">
                           <div class="popular-posts">
                              <div class="slider-wrapper js-slider">
                                 <div class="item">
                                    <a href="dai-dong-tien-va-hanh-trinh-lot-xac.html" title="Đại Đồng Tiến Và Hành Trình Lột Xác">
                                    <img src="../uploads/5/tin-cong-ty/4-750x420.jpg"
                                       alt="Đại Đồng Tiến Và Hành Trình Lột Xác"
                                       class="img-responsive">
                                    </a>
                                    <div class="caption">
                                       <span class="time"><i
                                          class="glyphicon glyphicon-time"></i> 26-08-2015</span>
                                       <h3 class="h3 js-truncate-text">Đại Đồng Tiến Và Hành Trình Lột Xác</h3>
                                    </div>
                                 </div>
                                 <div class="item">
                                    <a href="nhan-giai-thuong-hang-viet-nam-chat-luong-cao-lan-thu-19.html" title="Nhận Giải Thưởng Hàng Việt Nam Chất Lượng Cao Lần Thứ 19 (Năm 2015)">
                                    <img src="../uploads/5/tin-cong-ty/giaithuonghvnclc2-750x420.jpg"
                                       alt="Nhận Giải Thưởng Hàng Việt Nam Chất Lượng Cao Lần Thứ 19 (Năm 2015)"
                                       class="img-responsive">
                                    </a>
                                    <div class="caption">
                                       <span class="time"><i
                                          class="glyphicon glyphicon-time"></i> 27-02-2015</span>
                                       <h3 class="h3 js-truncate-text">Nhận Giải Thưởng Hàng Việt Nam Chất Lượng Cao Lần Thứ 19 (Năm 2015)</h3>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="tab-wrapper">
                              <!-- Nav tabs -->
                              <ul class="nav nav-tabs" role="tablist">
                                 <li role="presentation"
                                    class="active">
                                    <a data-target="#dai-dong-tien-news"
                                       role="tab"
                                       href="tin-tuc8b4e.html?tab=dai-dong-tien-news"
                                       data-toggle="tab">
                                    Tin Công Ty
                                    </a>
                                 </li>
                                 <li role="presentation"
                                    class="">
                                    <a data-target="#tin-khuyen-mai"
                                       role="tab"
                                       href="tin-tucb669.html?tab=tin-khuyen-mai"
                                       data-toggle="tab">
                                    Tin Khuyến Mãi
                                    </a>
                                 </li>
                              </ul>
                              <!-- Tab panes -->
                              <div class="tab-content">
                                 <div role="tabpanel"
                                    class="tab-pane active"
                                    id="dai-dong-tien-news">
                                    <div class="posts-list">
                                       <ul>
                                          @foreach($type_news_km as $item)
                                          <li>
                                             <div class="thumbnail">
                                                <a href="{{$item->slug}}"
                                                   title="{{$item->title_type}}">
                                                <img
                                                   src="uploads/5/tin-cong-ty/{{$item->image}}"
                                                   alt="$item->title_type">
                                                </a>
                                                <div class="date-time">
                                                   <span
                                                      class="date">03</span>
                                                   <span
                                                      class="month">03</span>
                                                   <span
                                                      class="year">2018</span>
                                                </div>
                                             </div>
                                             <div class="content">
                                                <a href="{{$item->slug}}"
                                                   title="{{$item->title_type}}"
                                                   class="h3">{{$item->title_type}}</a>
                                                <div class="description js-truncate-text">
                                                   {{$item->description}}
                                                </div>
                                                <a href="{{$item->slug}}"
                                                   class="btn-special"
                                                   title="{{$item->title_type}}">
                                                Xem thêm
                                                </a>
                                             </div>
                                          </li>
                                          @endforeach
                                       </ul>
                                    </div>
                                 </div>
                                 <div role="tabpanel"
                                    class="tab-pane "
                                    id="tin-khuyen-mai">
                                    <div class="posts-list">
                                       <ul>
                                          @foreach($type_news_ct as $item)
                                          <li>
                                             <div class="thumbnail">
                                                <a href="{{$item->slug}}"
                                                   title="{{$item->title_type}}">
                                                <img
                                                   src="uploads/5/tin-cong-ty/{{$item->image}}"
                                                   alt="$item->title_type">
                                                </a>
                                                <div class="date-time">
                                                   <span
                                                      class="date">03</span>
                                                   <span
                                                      class="month">03</span>
                                                   <span
                                                      class="year">2018</span>
                                                </div>
                                             </div>
                                             <div class="content">
                                                <a href="{{$item->slug}}"
                                                   title="{{$item->title_type}}"
                                                   class="h3">{{$item->title_type}}</a>
                                                <div class="description js-truncate-text">
                                                   {{$item->description}}
                                                </div>
                                                <a href="{{$item->slug}}"
                                                   class="btn-special"
                                                   title="{{$item->title_type}}">
                                                Xem thêm
                                                </a>
                                             </div>
                                          </li>
                                         @endforeach
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <aside class="sidebar">
                           <form action="https://www.daidongtien.com.vn/vi/tin-tuc.html" method="GET" class="search-form">
                              <div class="input-group">
                                 <input type="text"
                                    class="form-control"
                                    placeholder="Tìm kiếm..."
                                    value=""
                                    name="k">
                                 <div class="input-group-btn">
                                    <button class="btn btn-default" type="button">
                                    <i class="glyphicon glyphicon-search"></i>
                                    </button>
                                 </div>
                              </div>
                           </form>
                           <div class="well">
                              <div class="h4">Danh mục</div>
                              <div class="well-content">
                                 <ul class="list-unstyled categories-list">
                                    <li>
                                       <a href="tin-tuc8b4e.html?tab=dai-dong-tien-news">
                                       Tin Công Ty (28)
                                       </a>
                                    </li>
                                    <li>
                                       <a href="tin-tucb669.html?tab=tin-khuyen-mai">
                                       Tin Khuyến Mãi (6)
                                       </a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                           <div class="well">
                              <div class="h4">Tags</div>
                              <div class="well-content">
                                 <ul class="list-unstyled tags-list">
                                    <li>
                                       <a href="post-tag/nha-san-xuat-nhua-so-1.html" title="Nhà Sản Xuất Nhựa Số 1">
                                       Nhà Sản Xuất Nhựa Số 1
                                       </a>
                                    </li>
                                    <li>
                                       <a href="post-tag/dai-dong-tien.html" title="Dai Dong Tien">
                                       Dai Dong Tien
                                       </a>
                                    </li>
                                    <li>
                                       <a href="post-tag/finance.html" title="Finance">
                                       Finance
                                       </a>
                                    </li>
                                    <li>
                                       <a href="post-tag/pp.html" title="PP">
                                       PP
                                       </a>
                                    </li>
                                    <li>
                                       <a href="post-tag/world.html" title="World">
                                       World
                                       </a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                           <div class="well">
                              <div class="h4">BẠN CẦN TƯ VẤN?</div>
                              <div class="well-content">
                                 <form action="https://www.daidongtien.com.vn/contact-forms/send"
                                    method="POST"
                                    class="contact-form">
                                    <input type="hidden" name="_token" value="1k5vUJeQY9f7j0H5ABQuhGodNh1IK9Z72G5pK2Eb">
                                    <div class="form-group">
                                       <input type="text" class="form-control" name="name" placeholder="Tên *">
                                    </div>
                                    <div class="form-group">
                                       <input type="text" class="form-control" name="email" placeholder="Email *">
                                    </div>
                                    <div class="form-group">
                                       <input type="text" class="form-control" name="phone" placeholder="Số Điện Thoại">
                                    </div>
                                    <div class="form-group">
                                       <textarea name="content"
                                          rows="6"
                                          class="form-control"
                                          placeholder="Nội dung *"></textarea>
                                    </div>
                                    <div class="form-actions">
                                       <button type="submit" class="btn-special">Gửi tin nhắn</button>
                                       <a href="#" class="clear">Xóa <i class="glyphicon glyphicon-remove"></i></a>
                                    </div>
                                 </form>
                              </div>
                           </div>
                        </aside>
                     </div>
                  </section>
               </main>
@endsection