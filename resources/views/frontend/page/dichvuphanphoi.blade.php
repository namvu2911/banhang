@extends('frontend.master')
@section('content')
 <main class="main" id="main">
                  <section class="page-section page-title">
                     <div class="container">
                        <h1>&nbsp;</h1>
                        <ul class="breadcrumb">
                           <li><a title="Trang chủ" href="../vi.html">Trang chủ</a></li>
                           <li><span>Dịch vụ</span></li>
                           <li><span>Dịch vụ phân phối</span></li>
                        </ul>
                     </div>
                  </section>
                  <section class="page-section main-section">
                     <div class="main-content">
                        <div class="container">
                           <div class="title-with-description">
                              <h1 class="h1">DỊCH VỤ PHÂN PHỐI</h1>
                              <div class="description"></div>
                           </div>
                           <div class="services-container">
                              <img src="public/uploads/1/page-templates/service-distribution/service-dis-map.png"
                                 alt="Dịch vụ phân phối"
                                 class="img-responsive">
                              <ul class="services-list">
                                 <li class="number-left"
                                    data-color="blue">
                                    <div class="wrapper">
                                       <div class="number-box">
                                          <div class="number-content">
                                             <div class="h5">More than</div>
                                             <div class="h1 js-text-fill">
                                                <span>60</span>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="content-box">
                                          <div class="title">NHÀ PHÂN PHỐI</div>
                                          <div class="description">
                                             Chúng tôi sở hữu mạng lưới phân phối mạnh mẽ
                                          </div>
                                          <div class="icon">
                                             <img src="public/uploads/1/page-templates/service-distribution/service-dis-60.png"
                                                alt=""
                                                class="img-responsive"
                                                width="35"
                                                height="35">
                                          </div>
                                       </div>
                                    </div>
                                 </li>
                                 <li class="number-right"
                                    data-color="orange">
                                    <div class="wrapper">
                                       <div class="number-box">
                                          <div class="number-content">
                                             <div class="h5">More than</div>
                                             <div class="h1 js-text-fill">
                                                <span>10</span>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="content-box">
                                          <div class="title">CHUỖI SIÊU THỊ DANH TIẾNG</div>
                                          <div class="description">
                                             Coopmart, Big C, Maximart, Metro, Vinmart, Lotte, Aeon,..
                                          </div>
                                          <div class="icon">
                                             <img src="public/uploads/1/page-templates/service-distribution/service-dis-10.png"
                                                alt=""
                                                class="img-responsive"
                                                width="35"
                                                height="35">
                                          </div>
                                       </div>
                                    </div>
                                 </li>
                                 <li class="number-left"
                                    data-color="green">
                                    <div class="wrapper">
                                       <div class="number-box">
                                          <div class="number-content">
                                             <div class="h5">More than</div>
                                             <div class="h1 js-text-fill">
                                                <span>3,000</span>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="content-box">
                                          <div class="title">CỬA HÀNG</div>
                                          <div class="description">
                                             Hệ thống cửa hàng bán lẻ trên khắp cả nước với đội ngũ sale chuyên nghiệp
                                          </div>
                                          <div class="icon">
                                             <img src="public/uploads/1/page-templates/service-distribution/service-dis-3000.png"
                                                alt=""
                                                class="img-responsive"
                                                width="35"
                                                height="35">
                                          </div>
                                       </div>
                                    </div>
                                 </li>
                                 <li class="number-right"
                                    data-color="purple">
                                    <div class="wrapper">
                                       <div class="number-box">
                                          <div class="number-content">
                                             <div class="h5">More than</div>
                                             <div class="h1 js-text-fill">
                                                <span>63</span>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="content-box">
                                          <div class="title">TỈNH THÀNH TẠI VIỆT NAM</div>
                                          <div class="description">
                                             Sản phẩm của chúng tôi có mặt trên khắp các tỉnh thành
                                          </div>
                                          <div class="icon">
                                             <img src="public/uploads/1/page-templates/service-distribution/service-dis-30.png"
                                                alt=""
                                                class="img-responsive"
                                                width="35"
                                                height="35">
                                          </div>
                                       </div>
                                    </div>
                                 </li>
                              </ul>
                           </div>
                           <div class="services-with-images">
                              <ul class="services-list">
                                 <li>
                                    <div class="image"
                                       style="background-image: url('public/uploads/1/page-templates/service-distribution/service-dis-outstaff.png')">
                                    </div>
                                    <div class="quote">
                                       <div class="text">Đội ngũ sale chuyên nghiệp, nhiệt tình<br>và năng động.</div>
                                    </div>
                                 </li>
                                 <li>
                                    <div class="image"
                                       style="background-image: url('public/uploads/1/page-templates/service-distribution/service-dis-customerservice.png')">
                                    </div>
                                    <div class="quote">
                                       <div class="text">Dịch vụ chăm sóc khách hàng tận tâm <br>và thân thiện.</div>
                                    </div>
                                 </li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="content-with-background contact-form-container"
                        style="background-image: url('public/uploads/1/page-templates/service-distribution/service-dis-consult.png')">
                        <div class="title-with-description">
                           <h3 class="h1">BẠN CẦN TƯ VẤN?</h3>
                        </div>
                        <form action="https://www.daidongtien.com.vn/contact-forms/send"
                           method="POST"
                           class="contact-form"
                           enctype="multipart/form-data">
                           <input type="hidden" name="_token" value="1k5vUJeQY9f7j0H5ABQuhGodNh1IK9Z72G5pK2Eb">
                           <div class="wrapper">
                              <div class="form-group">
                                 <input type="text" class="form-control" name="name" placeholder="Tên *" required>
                              </div>
                              <div class="form-group">
                                 <input type="text" class="form-control" name="email" placeholder="Email *" required>
                              </div>
                              <div class="form-group">
                                 <input type="text" class="form-control" name="phone" placeholder="Số Điện Thoại">
                              </div>
                              <div class="form-group">
                                 <textarea name="content"
                                    rows="6"
                                    class="form-control"
                                    minlength="10"
                                    placeholder="Nội dung *" required></textarea>
                              </div>
                              <div class="form-group select-file-group">
                                 <div class="select-file-label">Đính kèm File:</div>
                                 <div class="custom-select-file">
                                    <label>
                                    <input type="file"
                                       class="form-control"
                                       name="attachments[]"
                                       multiple
                                       data-multiple-caption="{count} files selected">
                                    <span class="file-info">0 Tập tin được chọn</span>
                                    <span class="btn-file">Chọn</span>
                                    </label>
                                 </div>
                                 <div class="select-file-label">Tập tin hình ảnh, pdf, word, excel,... tải lên tối đa 2MB</div>
                              </div>
                              <div class="form-actions">
                                 <button type="submit" class="btn-special">Gửi tin nhắn</button>
                                 <a href="#" class="clear">
                                 Xóa <i class="glyphicon glyphicon-remove"></i>
                                 </a>
                              </div>
                           </div>
                        </form>
                     </div>
                     <div class="items-list-slider partners-list">
                        <div class="container">
                           <div class="title-with-description">
                              <h3 class="h1">Đối tác</h3>
                           </div>
                           <div class="js-slider-brands-list">
                              <div class="item">
                                 <img src="public/uploads/2/page-templates/doi-tac/web-01-3.png"
                                    alt="Unilever"
                                    class="">
                                 <img src="public/uploads/2/page-templates/doi-tac/web-09-1.png"
                                    alt="TOA"
                                    class="">
                              </div>
                              <div class="item">
                                 <img src="public/uploads/2/page-templates/doi-tac/web-04-1.png"
                                    alt="NESTLE"
                                    class="">
                                 <img src="public/uploads/2/page-templates/doi-tac/web-05-1.png"
                                    alt="Sofia"
                                    class="">
                              </div>
                              <div class="item">
                                 <img src="public/uploads/2/page-templates/doi-tac/web-08-1.png"
                                    alt="Vietnamobile"
                                    class="">
                                 <img src="public/uploads/2/page-templates/doi-tac/web-07-1.png"
                                    alt="Woolworths"
                                    class="">
                              </div>
                              <div class="item">
                                 <img src="public/uploads/2/page-templates/doi-tac/web-06-1.png"
                                    alt=""
                                    class="">
                                 <img src="public/uploads/2/page-templates/doi-tac/web-03-2.png"
                                    alt=""
                                    class="">
                              </div>
                              <div class="item">
                                 <img src="public/uploads/2/page-templates/doi-tac/web-09-1.png"
                                    alt=""
                                    class="">
                                 <img src="public/uploads/2/page-templates/doi-tac/web-04-1.png"
                                    alt=""
                                    class="">
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="items-list-slider certificates-list">
                        <div class="container">
                           <div class="title-with-description">
                              <h3 class="h1">Chứng nhận</h3>
                           </div>
                           <div class="js-slider-brands-list">
                              <div class="item">
                                 <img src="public/uploads/1/page-templates/service-distribution/service-dis-certifcates-iso9001-1-1.png"
                                    title="ISO 9001 : 2008"
                                    alt="ISO 9001 : 2008">
                                 <img src="public/uploads/1/page-templates/service-distribution/service-dis-certificates-14001.png"
                                    title="ISO 14001 : 2015"
                                    alt="ISO 14001 : 2015">
                              </div>
                              <div class="item">
                                 <img src="public/uploads/1/page-templates/service-distribution/service-dis-certificates-bsci.png"
                                    title="BSCI REPORT"
                                    alt="BSCI REPORT">
                                 <img src="public/uploads/1/page-templates/service-distribution/service-dis-certificates-fda-2.png"
                                    title="FDA CERTIFICATION"
                                    alt="FDA CERTIFICATION">
                              </div>
                              <div class="item">
                                 <img src="public/uploads/1/page-templates/service-distribution/service-dis-certificates-icti-3.png"
                                    title="ICTI CARE CERTIFICATION"
                                    alt="ICTI CARE CERTIFICATION">
                                 <img src="public/uploads/1/page-templates/service-distribution/service-dis-certificates-meta-1.png"
                                    title="SMETA 4 PILLARS STANDARD"
                                    alt="SMETA 4 PILLARS STANDARD">
                              </div>
                              <div class="item">
                                 <img src="public/uploads/1/page-templates/service-distribution/service-dis-certificates-lfbg-2.png"
                                    title="LFGB CERTIFICATION"
                                    alt="LFGB CERTIFICATION">
                                 <img src="public/uploads/1/page-templates/service-distribution/service-dis-certificates.png"
                                    title="VIET NAM’S TOP 500 LARGEST ENTERPRISES"
                                    alt="VIET NAM’S TOP 500 LARGEST ENTERPRISES">
                              </div>
                              <div class="item">
                                 <img src="public/uploads/1/page-templates/service-distribution/service-dis-certificates-vietnamclc.png"
                                    title="Hàng Việt Nam Chất lượng cao"
                                    alt="Hàng Việt Nam Chất lượng cao">
                                 <img src="public/uploads/1/page-templates/service-distribution/service-dis-certificates-cr.png"
                                    title="Chứng nhận hợp qui CR"
                                    alt="Chứng nhận hợp qui CR">
                              </div>
                           </div>
                        </div>
                     </div>
                  </section>
               </main>
@endsection